/*
 * FileUtils.java
 *
 * Created on 10/12/2009, 10:10:30
 *
 */
package tracx.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

/**
 *
 * @author eran
 */
public class FileUtils{

    /** The default encoding. */
    public static final String DEFAULT_ENCODING = "UTF-8";
    private static Logger logger = Logger.getLogger(FileUtils.class.getName());
    /**
     * No need to creates a new instance of FileUtils.
     *
     */
    private FileUtils(){
    }

    /**
     * Reads a file content into a string object.
     */
    public static String readFile(String fileName, String charset){

        StringBuilder buf = new StringBuilder();
        BufferedReader in = null;
        try
        {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),charset);
            in = new BufferedReader(isr);
            String s;

            while((s=in.readLine())!=null)
            {
                if(s.length()==0)
                {   // Skip empty lines.
                    continue;
                }

                buf.append(s);
                buf.append("\r\n");
            }

            in.close();
        }
        catch(IOException ex)
        {
        	if(logger.isLoggable(Level.WARNING))
			{
        		logger.warning("Couldn't access the file " +fileName);
			}
            return null;
        }
        finally
        {
        	IOUtils.closeQuietly(in);
        }

        return buf.toString();
    }

    /**
    * Lists the files in the given folder.
    * Any file that cannot be read is excluded.
    *
    * @param folder- the target folder.
    * @param extension- the extension of files to target.
    *
    * @return an array of files that match the conditions.
    */
   public static String[] list(String folder, final String extension){

        File dir = new File(folder);

        // This filter returns files, only.
        FileFilter filter = new FileFilter(){

            public boolean accept(File file){

                String name = file.getName();

                return file.isFile() && name.endsWith(extension);
            }
        };

        File[] children = dir.listFiles(filter);

        if(children==null)
        {
            return null;
        }

        int len = children.length;
        List<String> list = new ArrayList<String>(len);

        for(int i=0;i<len;i++)
        {
            File file = children[i];

            if(file.canRead())
            {
                String filename = file.getAbsolutePath();
                list.add(filename);
            }
        }

        if(list.size()==0)
        {
            return null;
        }

        String[] results = list.toArray(new String[]{});

        return results;
   }

   /**
    * Try to delete a given file.
    *
    * @param path- the path to the file.
    *
    * @return true iff the deletion has successfully ended.
    */
    public boolean delete(String path){

        File f = new File(path);
        boolean success = f.delete();

        return success;
    }

    /**
     * Try to rename a given file.
     *
     * @param path- the present name of the file.
     *
     * @return true iff the operational has successfully ended.
     */
    public boolean rename(String path, String npath){

        File f = new File(path);
        File nf = new File(npath);
        boolean success = f.renameTo(nf);

        return success;
    }
    
	/**
	 * Loads resources regardless if it's running from the server or from the servlet.
	 *
	 * @param path - The path to the specific file.
	 *
	 * @return Input stream of the file or null if file not found.
	 */
	public static InputStream loadResourceStream(String path)
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String classpath = path.replace(File.separatorChar, '/');

		InputStream stream = classLoader.getResourceAsStream(classpath);
		
		// For non servlet purposes
		if (stream == null)
		{
			try
			{
				stream = new FileInputStream(classpath);
			}
			catch (FileNotFoundException e)
			{
	        	if(logger.isLoggable(Level.WARNING))
				{
	        		logger.warning("loadResourceStream: Could not load stream "+ path);
				}
				return null;
			}
		}

		return stream;
	}

	/**
	 * Loads resources regardless if it's running from the server or from the servlet.
	 * @param path - The path to the specific file.
	 */
	public static File getResource(String path)
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String unixpath = path.replace(File.separatorChar, '/');

		URL resource = classLoader.getResource(unixpath);

		try
		{
			return (resource == null) ? new File(path) : new File(resource.toURI());
		}
		catch(URISyntaxException e)
		{
        	if(logger.isLoggable(Level.WARNING))
			{
        		logger.warning("getResource: " + e.getMessage());
			}
        	return  null;
		}
	}

	/**
	 * Loads resources regardless if it's running from the server or from the servlet.
	 * @param path - The path to the specific file.
	 */
	public static URL getResourceAsURL(String path)
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		String unixpath = path.replace(File.separatorChar, '/');

		URL resource = classLoader.getResource(unixpath);

		try
		{
			return (resource == null) ? new URL(new URL("file:")+path) : resource;
		}
		catch(Exception e)
		{
        	if(logger.isLoggable(Level.WARNING))
			{
        		logger.warning("getResourceAsURL: " + e.getMessage());
			}
			return null;
		}
	}

	
}
