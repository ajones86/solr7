/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Commons                                          //
//  © 2006-2015 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	02/08/2012	Niv				Added getHTMLAsText that allows to keep cookies between calls (in the same thread) and clearCookies()
//	07/12/2011	Niv				Added containsSessionQueryParams, removeSessionQueryParams, removeQueryParameters (multiple)
//	02/10/2011	Niv				Added headers to setConnectionParameters, getStreamAfterRedirections, getHTMLAsText
//	12/09/2011	Niv				Added getDNSAttributes, nslookup
//	22/02/2009	Niv				Added getSHA1
//	02/02/2009	Niv				Added parseLong, parseInt
//  02/02/2009  Ofer            Add getAbsoluteUrl and isEmpty
//  27/01/2009  Ofer            Add getQueryParameter and getHTTPInputStreamAsText with different encodings
//  25/01/2009  Ofer            Add createHashMap
//	22/01/2009	Niv				Added closeInputStream(), getBasicAuth()
//	25/12/2008	Niv				DateFormat is not thread-safe! God damn it!
//	14/07/2008	Niv				Added unescapeHtml(); Use decodeNumericCharacterReferences() in addition to StringEscapeUtils
//	03/04/2008	Niv				Added convertDateTimeToDate()
//  30/04/2008	Noam			Added getLastItem()
//	23/04/2008	Niv				Throw IOException caught in getHTTPInputStream; Added parseIOExceptionResponseCode; Added proxy param to getHTTPInputStream
//	22/04/2008	Niv				Added parseDCDate()
//	19/04/2008	Niv				Moved MD5Helper method here
//	19/04/2008	Noam			Added getHTTPInputStream()
//	16/04/2008	Noam			Added stringFromInputStream()
//	12/03/2008	Niv				Added getFirstKey(), getFirstValue()
//	08/03/2008	Niv				Added parseDate()
//	03/03/2008	Niv				Added convertDateToUnixTime(), convertUnixTimeToDate()
//	23/02/2008	Noam			Made safeCompare() generic
//	20/02/2008	Niv				Added safeCompare()
//	22/01/2008	Niv				Added parseShort()
//	15/01/2008	Niv				Added now(), formatDate()
//	07/12/2007	Niv				Created
//
//

package tracx.common.utils;

import etm.core.configuration.EtmManager;
import etm.core.renderer.SimpleTextRenderer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import tracx.common.tracing.Trace;
import tracx.server.Configuration;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.net.*;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

public class Helpers
{
	
	/**
	 * Loads resources regardless if it's running from the server or from the servlet.
	 *
	 * @param path - The path to the specific file.
	 *
	 * @return Input stream of the file or null if file not found.
	 */
	public static InputStream loadResourceStream(String path)
	{
		return FileUtils.loadResourceStream(path);
	}
	
	/**
	 * Loads resources regardless if it's running from the server or from the servlet.
	 * @param path - The path to the specific file.
	 */
	public static URL getResourceAsURL(String path)
	{
		return FileUtils.getResourceAsURL(path);
	}

	public static String convertInputStreamToString(InputStream stream) throws IOException
	{
		if(stream == null) return null;
		InputStreamReader input = new InputStreamReader(stream);
		StringWriter output = new StringWriter();
		char[] buffer = new char[1024];
		//long count = 0;
		int n = 0;
		while (-1 != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			//count += n;
		}
		return output.toString();
	}	
}
