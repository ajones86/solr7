/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	03/08/2009	Niv				Added IndexDirectory
//	07/01/2009	Niv				Initialize TwitterRest
//	30/12/2008	Ofer			Initialize FacebookFactory
//	06/05/2008	Niv				Configurable number of concurrent workers
//	01/03/2008	Niv				Initialize JCS before FlickrFactory
//	28/02/2008	Niv				Initialize YouTubeFactory
//	27/01/2007	Niv				Initialize JCS
//	10/12/2007	Niv				Created
//
//

package tracx.server;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.jcs.engine.control.CompositeCacheManager;
import etm.contrib.renderer.plugin.Log4jDumpOnShutdownPlugin;
import etm.core.aggregation.Aggregator;
import etm.core.aggregation.BufferedThresholdAggregator;
import etm.core.aggregation.NotifyingAggregator;
import etm.core.aggregation.RootAggregator;
import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import tracx.common.tracing.Trace;
import tracx.common.tracing.metrics.TracxMetrics;
import tracx.common.tracing.metrics.TracxMetricsEtmPlugin;

public class Configuration
{
//	public static final boolean FORCE_SHALLOW_UPDATE = false;

    /** The default path to the config files. */
	public final static String CONFIGURATION_FILE = "./config/tra.cx.server.properties";
    /** The default path to resource files. */
    public final static String RESOURCE_PATH = "./prop/";
//    /** The default extension of resource files. */
//    public final static String RESOURCE_EXTENSION = ".txt";

	private static boolean ms_initialized = false;
	private static boolean ms_initializing = false;

    /** Holds the properties object. */
    private static PropertiesConfiguration configuration = new PropertiesConfiguration();

//    /** Holds resources. */
//    private static Map<String,String> resources = new HashMap<String,String>();
//    /** Holds objects. */
//    private static Map<String,Object> objects = new HashMap<String,Object>();

	private Configuration()
	{
	}

//    /**
//     * Fetches a resource data by its tied key.
//     *
//     * @param key- the key to use. For instance: 'sentiment_heb_food'.
//     *
//     * @return a string object which represents the resource.
//     */
//    public static String fetchResource(String key)
//    {
//        String resource = resources.get(key);
//
//        if(resource==null)
//        {
//            StringBuilder buf = new StringBuilder();
//            buf.append(RESOURCE_PATH);
//            buf.append(key);
//            buf.append(RESOURCE_EXTENSION);
//
//            resource = FileUtils.readFile(buf.toString(),FileUtils.DEFAULT_ENCODING);
//
//            if(resource!=null)
//            {   // Notice: resource data is small in size.
//                resources.put(key,resource);
//            }
//        }
//
//        return resource;
//    }
//
//    /**
//     * Introduces a new object here.
//     *
//     * @param o- the object to store.
//     * @param key- the key to tie to the object.
//     */
//    public static void storeObject(Object o, String key)
//    {
//        objects.put(key,o);
//    }
//
//    /**
//     * Fetches a object by its tied key.
//     *
//     * @param key- the key to use. For instance: 'sentiment_heb_food'.
//     *
//     * @return an object by its tied key.
//     */
//    public static Object fetchOject(String key)
//    {
//        Object object = objects.get(key);
//
//        return object;
//    }

	public static boolean isInitialized()
	{
		return ms_initialized;
	}

	public static void initialize()
	{
		initialize(CONFIGURATION_FILE);
	}

    public static void initialize(String configurationFile)
    {
        initialize(configurationFile, null, null);
    }

	public static void initialize(String configurationFile, String consolePattern, String filePattern)
	{
		if(ms_initialized) return;

		if(configurationFile == null)
        {
            throw new IllegalArgumentException("configurationFile is null");
        }

		synchronized(Configuration.class)
		{
			if(ms_initializing)
			{
				System.err.println("Configuration initialization loop");
				//(new IllegalStateException("Configuration initialization loop")).printStackTrace();
				throw new IllegalStateException("Configuration initialization loop");
			}
			ms_initializing = true;
		}

		initializeTimeZone();
		Locale.setDefault(Locale.ENGLISH);

        try
        {
        	configuration.setDelimiterParsingDisabled(true);
	    	configuration.load(configurationFile);
        }
        catch(Exception e)
        {
        	// Tracing is not initialized at this point...
//			if(Trace.error())
//			{
//				Trace.error("Exception caught while trying to read configuration file '%s': %s", e, configurationFile, e.getMessage());
//			}
//			else
//			{
        		String currentDir;
        		try
        		{
        			currentDir = Paths.get("").toAbsolutePath().toString();
        		}
        		catch(Exception e1)
        		{
        			currentDir = "[failed obtaining]";
        		}
            	System.err.println(String.format("Exception caught while trying to read configuration file '%s' (current dir is '%s'): %s", configurationFile, currentDir, e.getMessage()));
            	e.printStackTrace();
//			}

            return;
        }

        Trace.initialize(configuration, consolePattern, filePattern);
        TracxMetrics.configure(configuration);
    	cacheConfiguration();

//    	if(configuration.getBoolean("etm_manager", false))
//    	{
//    		boolean jetm_metrics = TracxMetrics.isJETMMetricsEnabled();
//    		Aggregator aggregator = jetm_metrics ? new BufferedThresholdAggregator(new NotifyingAggregator(new RootAggregator())) : null;	//BufferedTimedAggregator
//
//    		BasicEtmConfigurator.configure(false, aggregator);
//    		EtmMonitor monitor = EtmManager.getEtmMonitor();
//    	    monitor.addPlugin(new Log4jDumpOnShutdownPlugin());
//    	    if(jetm_metrics) monitor.addPlugin(new TracxMetricsEtmPlugin());
//    	    monitor.start();
//    	    Runtime.getRuntime().addShutdownHook(new Thread() {
//    	        @Override
//    			public void run()
//    	        {
//    	        	if(EtmManager.getEtmMonitor() != null)
//    	        		EtmManager.getEtmMonitor().stop();
//    	        }
//    	    });
//    	}

    	ms_initialized = true;
	}

	public static void shutdown()
	{
		TracxMetrics.shutdown();
	}


    /**
     * Fetches a property which is tied to the given key.
     *
     * @param key- the to use.
     *
     * @return the property tied to the given key, or null is no such exists.
     */
    public static String getProperty(String key)
    {
    	initialize();
        return configuration.getString(key);
    }

    /**
     * Fetches a property which is tied to the given key.
     *
     * @param key- the to use.
     * @param defaultValue- the to use in case the value doesn't exist.
     *
     * @return the property tied to the given key, or default value if is no such exists.
     */
    public static String getProperty(String key, String defaultValue)
    {
    	initialize();
        return configuration.getString(key, defaultValue);
    }

    public static int getIntProperty(String key, int defaultValue)
    {
    	initialize();
    	return configuration.getInteger(key, defaultValue);
    }

    public static double getDoubleProperty(String key, double defaultValue)
    {
    	initialize();
    	return configuration.getDouble(key, defaultValue);
    }

    public static float getFloatProperty(String key, float defaultValue)
    {
    	initialize();
    	return configuration.getFloat(key, defaultValue);
    }

    public static long getLongProperty(String key, long defaultValue)
    {
    	initialize();
    	return configuration.getLong(key, defaultValue);
    }

    public static boolean getBooleanProperty(String key, boolean defaultValue)
    {
    	initialize();
    	return configuration.getBoolean(key, defaultValue);
    }

    public static void setProperty(String key, Object value)
    {
    	initialize();
    	configuration.setProperty(key, value);
    }


	private static void initializeTimeZone()
	{
		System.setProperty("user.timezone", "UTC");
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	private static void cacheConfiguration()
	{
		Iterator<?> jcsKeys = configuration.getKeys("jcs");

		//when overriding values (in additional.properties for example), it doesn't really override but creates an ArrayList of values, and the cache doesn't handle it,
		//so we extract the first value (the overriden one)
		while(jcsKeys.hasNext())
		{
			String key = (String)jcsKeys.next();
			Object value = configuration.getProperty(key);

			if(value instanceof ArrayList)
			{
				configuration.setProperty(key, ((ArrayList<?>)value).get(0));
			}
		}

		CompositeCacheManager ccm = CompositeCacheManager.getUnconfiguredInstance();
		ccm.configure(ConfigurationConverter.getProperties(configuration));
	}
}
