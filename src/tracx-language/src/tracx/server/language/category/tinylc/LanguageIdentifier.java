/*
 * LanguageIdentifier.java
 *
 * Created on 17/02/2010, 12:22:57
 *
 */
package tracx.server.language.category.tinylc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import tracx.server.Configuration;

/**
 *
 * @author eran
 */
public class LanguageIdentifier{

    /** The path taken from configuration. */
    protected static String path = Configuration.getProperty("lc_tiny_path", "./prop/models_tiny");
    /** The label tied to the result. */
	private String language = null;
	/** The outcome. */
    private double distance = 0.0;
    /** The available language profiles. */
    private static final Map<String,LanguageProfile> profiles = new HashMap<String,LanguageProfile>();
    /** The available language profiles. */
    private static final Map<String,TermsProfile> tprofiles = new HashMap<String,TermsProfile>();

    static
    {
    	loadLanguages(path);
    }

    //el-Greek
    //fi-Finnish
    //hu-Hungarian
    //is-Icelandic
    //nl-Dutch
    //no-Norwegian
    //pl-Polish
    //pt-Portuguese
    //sv-Swedish
    //th-Thai

    /**
     *
     *
     * @param profile
     */
    public LanguageIdentifier()
    {
    }

    /**
     *
     * @param content- the content.
     *
     * @return the language.
     */
    public String getLanguage(String content){

        LanguageProfile profile = new LanguageProfile(content);

        String minLanguage = "unknown";
        double minDistance = 1.0;

        Iterator<String> iter = profiles.keySet().iterator();

        while(iter.hasNext())
		{
            String l = iter.next();
            LanguageProfile lp = profiles.get(l);
            double d = profile.distance(lp);

            TermsProfile tp = tprofiles.get(l);

            if(tp!=null)
            {
                double d1 = 0.2*tp.getResult(content);
                d*=0.8;
                d+=d1;
            }

            if(d<minDistance)
			{
                minDistance = d;
                minLanguage = l;
            }
        }

        language = minLanguage;
        distance = minDistance;

        return language;
    }

    /**
     *
     * @return
     */
    public boolean isReasonablyCertain(){

        return distance<0.022;
    }

    private static void loadLanguages(String path)
    {
    	File[] files = new File(path).listFiles(new FilenameFilter(){
			public boolean accept(File dir, String name)
			{
				return name.endsWith(".ngp");
			}
		});
		final int n = files.length;
		for ( int i = 0; i < n; i++ )
		{
			addProfile(files[i].getName(), path);
		}
    }
    /**
     *
     * @param language
     */
    private static void addProfile(String languageFilename, String path){

        try
		{
        	String language = languageFilename.substring(0, languageFilename.indexOf("."));
            LanguageProfile profile = new LanguageProfile();

            File file = new File(path,languageFilename);
            InputStream stream = new FileInputStream(file);

            try
			{
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream,"UTF-8"));
                String line;

                while((line=reader.readLine())!=null)
				{
                    if((line.length()>0) && !line.startsWith("#"))
					{
                        int space = line.indexOf(' ');
                        profile.add(line.substring(0,space),Long.parseLong(line.substring(space+1)));
                    }
                }
            }
			finally
			{
                stream.close();
            }

            profiles.put(language,profile);

            TermsProfile tp = new TermsProfile();
            tp.initProfile(language,path);
            tprofiles.put(language,tp);

        }
		catch(Throwable t)
		{
            // Failed to load this language profile. Log the problem?
        }
    }

    @Override
    public String toString(){

        return language + " (" + distance + ")";
    }
}
