/*
 * TermsProfile.java
 *
 * Created on 21/02/2010, 15:52:42
 *
 */
package tracx.server.language.category.tinylc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author eran
 */
public class TermsProfile extends HashMap<String,Integer>{

    /** Holds the default delimiter. */
    protected String delimiter = "\r\n\t ;.,:?!\"(){}[]1234567890-—_“”«»*&<>|";
    /** Holds the total number of freqs here. */
    protected int total = 0;
    /** Holds the id of the language. */
    protected String id;

    /** 
     * Creates a new instance of TermsProfile.
     *
     */
    public TermsProfile(){
    }

    /**
     * Set a new delimiter here.
     *
     * @param delimiter- the new delimiter.
     */
    public void setDelimiter(String delimiter){

        this.delimiter = delimiter;
    }

    /**
     * Initiates this term profile object.
     *
     * @param id- the id of the language.
     */
    public void initProfile(String id, String path){

        InputStream stream = null;

        try
        {
            File file = new File(path,id + ".tf");
            stream = new FileInputStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream,"UTF-8"));
            String line;

            while((line=reader.readLine())!=null)
            {
                if(line.length()>0 && !line.startsWith("#"))
                {
                    int space = line.indexOf(' ');
                    String f = line.substring(space+1);
                    int freq = Integer.parseInt(f);
                    String term = line.substring(0,space);
                    put(term,freq);

                    total+=freq;
                }
            }

            this.id = id;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                stream.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Fetches the id of the language here.
     *
     * @return the id of the language here.
     */
    public String getID(){

        return id;
    }

    /**
     * Gets the score that the specified text gets here.
     *
     * @param text- the text to test.
     * 
     * @return the score that the specified text gets here.
     */
    public double getResult(String text){

        double result = 1.0;
        StringTokenizer tokenizer = new StringTokenizer(text,delimiter,false);
        List<String> terms = new ArrayList<String>();
        
        while(tokenizer.hasMoreTokens())
        {
            String term = tokenizer.nextToken().toLowerCase();
            terms.add(term);
        }

        int ttotal = terms.size();

        for(int i=0;i<ttotal;i++)
        {
            String term = terms.get(i);

            boolean exists = containsKey(term);
            result -= !exists ? 0 : 1/(double)ttotal;
        }

        return result<0 ? 0 : result;
    }
}
