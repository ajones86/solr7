 /*
 * ProfilingWriter.java
 *
 * Created on 17/02/2010, 12:26:02
 *
 */
package tracx.server.language.category.tinylc;

import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author eran
 */
public class ProfilingWriter extends Writer{

    /**  */
    private final LanguageProfile profile;
	/**  */
    private char[] buffer = new char[] {0,0,'_'};
	/**  */
    private int n = 1;
    /** The boosted flag. */
    protected boolean boosted = false;

    /**
     * 
     */
	public ProfilingWriter(){

        this(new LanguageProfile());
    }

    /**
     * 
     * @param profile
     */
    public ProfilingWriter(LanguageProfile profile){

        this.profile = profile;
    }

    /**
     * Returns the language profile being built by this writer. Note that
     * the returned profile gets updated whenever new characters are written.
     * Use the {@link #getLanguage()} method to get the language that best
     * matches the current state of the profile.
     *
     * @return language profile
     */
    public LanguageProfile getProfile(){

        return profile;
    }

    @Override
    public void write(char[] cbuf, int off, int len){

        for(int i=0;i<len;i++)
		{
            char c = Character.toLowerCase(cbuf[off + i]);

            if(Character.isLetter(c))
			{
                if(boosted)
                {
                    addBoostedLetter(c);
                }
                else
                {
                    addLetter(c);
                }
            }
			else
			{
                if(boosted)
                {
                    addBoostedSeparator();
                }
                else
                {
                    addSeparator();
                }
            }
        }
    }

    /**
     * 
     * @param c- the letter.
     */
    private void addLetter(char c){

        System.arraycopy(buffer,1,buffer,0,buffer.length-1);
        buffer[buffer.length-1] = c;
        n++;

        if(n>=buffer.length)
		{
            profile.add(new String(buffer));
        }
    }

    /**
     * 
     * @param c
     */
    private void addBoostedLetter(char c){

        System.arraycopy(buffer,1,buffer,0,buffer.length-1);
        buffer[buffer.length-1] = c;
        n++;

        if(n>=buffer.length)
		{
            String s = new String(buffer);
            boolean b = s.indexOf('_')>-1;
            profile.add(s,b ? 1 : 1);
        }
    }

    /**
     *
     */
    protected void addSeparator(){

        addLetter('_');

        n = 1;
    }

    /**
     *
     */
    protected void addBoostedSeparator(){

        System.arraycopy(buffer,1,buffer,0,buffer.length-1);
        buffer[buffer.length-1] = '_';
        n++;

        if(n>=buffer.length)
		{
            profile.add(new String(buffer),1);
        }

        n = 1;
    }

    @Override
    public void close() throws IOException{

        addSeparator();
    }

    /**
     * Ignored.
     */
    @Override
    public void flush(){
    }

    /**
     * The boosted flag.
     * Use the boosted flag when dealing with very short text.
     *
     * @param boosted- the large/small flag.
     */
    public void setBoosted(boolean boosted){

        this.boosted = boosted;
    }
}
