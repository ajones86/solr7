/*
 * LanguageProfile.java
 *
 * Created on 17/02/2010, 12:24:45
 *
 */
package tracx.server.language.category.tinylc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author eran
 */
public class LanguageProfile{

    /**  */
    public static final int DEFAULT_NGRAM_LENGTH = 3;
	/**  */
    private final int length;
    /** The ngrams that make up this profile. */
    private final Map<String,Counter> ngrams = new HashMap<String,Counter>();
    /** The sum of all ngram counts in this profile. Used to calculate relative ngram frequency. */
    private long count = 0;

    public LanguageProfile(int length){

        this.length = length;
    }

    public LanguageProfile(){

        this(DEFAULT_NGRAM_LENGTH);
    }

    /**
     * 
     * @param content
     * @param length
     */
    public LanguageProfile(String content, int length, boolean large){

        this(length);

        ProfilingWriter writer = new ProfilingWriter(this);
        writer.setBoosted(!large);
        char[] ch = content.toCharArray();
        writer.write(ch,0,ch.length);
    }

    /**
     *
     * @param content
     */
    public LanguageProfile(String content){

        this(content,DEFAULT_NGRAM_LENGTH,false);
    }

    /**
     *
     * @param content
     */
    public LanguageProfile(String content, boolean large){

        this(content,DEFAULT_NGRAM_LENGTH,large);
    }

    /**
     *
     * @return
     */
    public long getCount(){

        return count;
    }

    /**
     *
     */
    public int size(){

        return ngrams.size();
    }

    /**
     *
     * @param ngram
     * @return
     */
    public long getCount(String ngram){

        Counter counter = ngrams.get(ngram);

        if(counter!=null)
		{
            return counter.getCount();
        }
		else
		{
            return 0;
        }
    }

    /**
     * Adds a single occurrence of the given ngram to this profile.
     *
     * @param ngram- the ngram.
     */
    public void add(String ngram){

        add(ngram,1);
    }

    /**
     * Adds multiple occurrences of the given ngram to this profile.
     *
     * @param ngram- the ngram
     * @param count- number of occurrences to add
     */
    public void add(String ngram, long count){

        if(length!=ngram.length())
		{
            throw new IllegalArgumentException("Unable to add an ngram of incorrect length: " + ngram.length() + " != " + length);
        }

        Counter counter = ngrams.get(ngram);

        if(counter==null)
		{
            counter = new Counter();
            ngrams.put(ngram,counter);
        }

        long c = counter.getCount();
        counter.setCount(c+count);
        this.count += count;
    }

    /**
     *
     *
     */
    public Map<String,Counter> getNGrams(){

        return ngrams;
    }

    /**
     * Calculates the geometric distance between this and the given
     * other language profile.
     *
     * @param that the other language profile.
     *
     * @return distance between the profiles.
     */
    public double distance(LanguageProfile that){

        if(length!=that.length)
		{
            throw new IllegalArgumentException("Unable to calculage distance of language profiles"+ " with different ngram lengths: " + that.length + " != " + length);
        }

        boolean isThat = that.size()>size();

        double sumOfSquares = 0.0;
        double thisCount = Math.max(this.count,1.0);
        double thatCount = Math.max(that.count,1.0);

        Set<String> ngrams = new HashSet<String>();

        if(isThat)
        {
            ngrams.addAll(this.ngrams.keySet());
        }
        else
        {
            ngrams.addAll(that.ngrams.keySet());
        }

        Iterator<String> iter = ngrams.iterator();

        while(iter.hasNext())
		{
            String ngram = iter.next();
            boolean boost = ngram.indexOf('_')>-1;
            double thisFrequency = this.getCount(ngram) / thisCount;
            double thatFrequency = that.getCount(ngram) / thatCount;
            double difference = thisFrequency - (thatFrequency * (boost ? 2 : 1));

            sumOfSquares += difference * difference;
        }

        return Math.sqrt(sumOfSquares);
    }

    /**
     * 
     * @return
     */
    @Override
    public String toString(){

        return ngrams.toString();
    }
}
