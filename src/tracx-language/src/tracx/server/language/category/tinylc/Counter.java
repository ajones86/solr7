/*
 * Counter.java
 *
 * Created on 17/02/2010, 17:18:14
 *
 */
package tracx.server.language.category.tinylc;

/**
 *
 * @author eran
 */
public class Counter{

    /** The total number of times terms within the corpus here. */
    protected long count = 0;

    /** 
     * Creates a new instance of Counter.
     *
     */
    public Counter(){
    }

    /**
     * Introduces a new number of times of terms within the corpus.
     *
     * @param count- the new value.
     */
    public void setCount(long count){

        this.count = count;
    }

    /**
     * Fetches number of times of terms within the corpus.
     * 
     * @return number of times of terms within the corpus.
     */
    public long getCount(){

        return count;
    }

    /**
     * Fetches the count value as a string object.
     *
     * @return the count value as a string object.
     */
    @Override
    public String toString(){

        return ""+count;
    }

}
