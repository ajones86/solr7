/*
 * LanguageCategory.java
 *
 * Created on 16/11/2009, 11:49:39
 *
 */
package tracx.server.language.category;

import java.io.File;
import java.lang.Character.UnicodeBlock;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import tracx.common.tracing.Trace;
import tracx.server.Configuration;

/**
 *
 * @author eran
 */
public class LanguageCategory{

	/** The default language. */
	private static final String UNRECOGNIZED = "unrecognized";
	private static final int MIN_TEXT_LENGTH = 20;
	private static final int MAX_TEXT_LENGTH = 200;

	private static final Pattern ARABIC_PATTERN = Pattern.compile("[ة]");
	private static final Pattern FARSI_PATTERN = Pattern.compile("[چگژپ]");

	private static Set<Character.UnicodeBlock> ms_possibleLang;
	private static final String PROFILE_PATH = "./prop/profiles";
	private static boolean ms_isInitialize = false;
	private static final Pattern JAPANESE_PATTERN = Pattern.compile("\\p{InHiragana}|\\p{InKatakana}");
	private static final Pattern KOREAN_PATTERN = Pattern.compile("\\p{InHangulSyllables}");

	static
	{
		init();
	}

	private static void init()
	{
		try
		{
			loadPossibleLangMap();
			setLanguageDetector();

			ms_isInitialize = true;
		}
		catch(Exception e)
		{
			if(Trace.fatal()) Trace.fatal("LanguageCatagory.init: Failed to init - '%s'", e, e.getMessage());
		}
	}

	private static void setLanguageDetector() throws LangDetectException, URISyntaxException
	{
		URL resource =  Thread.currentThread().getContextClassLoader().getResource(PROFILE_PATH/*.replace(File.separatorChar, '/')*/);
		if(resource != null)
		{
			DetectorFactory.loadProfile(new File(resource.toURI()));
		}
		else
		{
			DetectorFactory.loadProfile(PROFILE_PATH);
		}

		DetectorFactory.setSeed(0);						// Don't use random for the lang detection.
	}

	private static void loadPossibleLangMap()
	{
		ms_possibleLang = new HashSet<Character.UnicodeBlock>();
		ms_possibleLang.add(Character.UnicodeBlock.ARABIC);
		ms_possibleLang.add(Character.UnicodeBlock.ARABIC_PRESENTATION_FORMS_A);
		ms_possibleLang.add(Character.UnicodeBlock.ARABIC_PRESENTATION_FORMS_B);
		ms_possibleLang.add(Character.UnicodeBlock.ARMENIAN);
		ms_possibleLang.add(Character.UnicodeBlock.BASIC_LATIN);
		ms_possibleLang.add(Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS);
		ms_possibleLang.add(Character.UnicodeBlock.GEORGIAN);
		ms_possibleLang.add(Character.UnicodeBlock.GREEK);
		ms_possibleLang.add(Character.UnicodeBlock.GREEK_EXTENDED);
		ms_possibleLang.add(Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO);
		ms_possibleLang.add(Character.UnicodeBlock.HANGUL_JAMO);
		ms_possibleLang.add(Character.UnicodeBlock.HANGUL_SYLLABLES);
		ms_possibleLang.add(Character.UnicodeBlock.HEBREW);
		ms_possibleLang.add(Character.UnicodeBlock.HIRAGANA);
		ms_possibleLang.add(Character.UnicodeBlock.THAI);
		ms_possibleLang.add(Character.UnicodeBlock.CYRILLIC);
	}

	//	public static Character.UnicodeBlock unicodeLanguageFinderNew(String text)
	//	{
	//		return unicodeLanguageFinderNew(text, false);
	//	}

	public static Character.UnicodeBlock unicodeLanguageFinderNew(String text)
	{
		if(text == null) throw new IllegalArgumentException("text is null");

		int maxCharsToCheck = 100;
		Character.UnicodeBlock block = null;
		for(int i = 0 ; ((i < text.length()) && (i < maxCharsToCheck)) ; i++)
		{
			char letter = text.charAt(i);
			block = Character.UnicodeBlock.of(letter);
			if((block == null) || !ms_possibleLang.contains(block)) // || (block.toString().contains("ARABIC") && !allowArabic))
			{
				block = null;
				continue;
			}

			if((block.equals(Character.UnicodeBlock.BASIC_LATIN)) || (block.equals(Character.UnicodeBlock.CYRILLIC)))
			{
				block = null;
				continue;
			}

			break;
		}

		return block;
	}


	private static String getReadableLangFromUnicodeBlock(Character.UnicodeBlock block)
	{
		if(block.equals(Character.UnicodeBlock.HEBREW))		return "he";
		if(block.toString().contains("CJK")) 				return "zh-cn";
		if(block.equals(Character.UnicodeBlock.HIRAGANA)) 	return "ja";
		if(block.equals(Character.UnicodeBlock.KATAKANA)) 	return "ja";
		if(block.equals(Character.UnicodeBlock.DEVANAGARI)) return "hi";
		if(block.toString().contains("HANGUL")) 			return "ko";
		if(block.toString().contains("ARABIC")) 			return "ar";
		if(block.equals(Character.UnicodeBlock.SYRIAC)) 	return "ar";
		if(block.toString().contains("GREEK")) 				return "el";
		if(block.equals(UnicodeBlock.THAI))					return "th";
		if(block.equals(UnicodeBlock.ARMENIAN))				return "hy";
		if(block.equals(UnicodeBlock.GEORGIAN))				return "ge";

		return block.toString().toLowerCase();
	}


	/**
	 * Finds the language in which the input string is most likely written.
	 * A default charset of UTF-8, is used.
	 *
	 * This method is thread safe.
	 *
	 * @param text- the text string to test.
	 *
	 * @return the language in which the input string is most likely written.
	 */
	public static String findLanguage(String text)
	{
		if(!ms_isInitialize)
		{
			if(Trace.error()) Trace.error("LanguageCategort.findLanguage: LanguageCategory failed to init - returning unrecognized for all texts.");
			return UNRECOGNIZED;
		}

		// First try and find the language according to the encoding of the text - this way is much faster.
		Character.UnicodeBlock block  = unicodeLanguageFinderNew(text);

		if((block != null))
		{
			Matcher matcher = null;
			// Both Persian and Arabic are identified as Arabic but there's a chance to separate between the two with special chars.
			if(block.toString().contains("ARABIC"))
			{
				matcher = ARABIC_PATTERN.matcher(text);
				if(matcher.find())
				{
					return "ar";
				}

				matcher = FARSI_PATTERN.matcher(text);
				if(matcher.find())
				{
					return "fa";
				}
				// If we were not able to detect the Arabic dialect directly from the chars we will let
				// the lang detect algorithm to decide.
				block = null;			
			}
			else if(block.toString().contains("CJK"))//special treatment for CJK block
			{
				//check whether text contains Japanese characters
				matcher = JAPANESE_PATTERN.matcher(text);
				if(matcher.find())
				{
					return "ja";
				}
				
				//check whether text contains Korean characters
				matcher = KOREAN_PATTERN.matcher(text);
				if(matcher.find())
				{
					return "ko";
				}
				// If we were not able to detect the Japanese/Korean directly from the chars we will let
				// the lang detect algorithm to decide.
				block = null;
			}
		}


		// If the language detected by the unicode language filter is not one of the languages
		// that need to be detected using the corpus - return it.
		if((block != null) && !block.equals(Character.UnicodeBlock.BASIC_LATIN) && !block.equals(Character.UnicodeBlock.CYRILLIC))
		{
			return getReadableLangFromUnicodeBlock(block);
		}

		// Cases where the text is too short - append it to itself - this heuristics was found
		// to improve language detection for short texts (Twitter).

		if(text.length() < MIN_TEXT_LENGTH)
		{
			text = extendToMinimunTextLength(text);
		}

		return detectLanguage(text.toLowerCase());
	}

	public static String extendToMinimunTextLength(String text)
	{
		StringBuilder extendedText = new StringBuilder(text);
		while(extendedText.length() < MIN_TEXT_LENGTH)
		{
			extendedText.append(" " + text);
		}

		return extendedText.toString();
	}

	private static String detectLanguage(String text)
	{
		if(true)
			return UNRECOGNIZED;

		try
		{
			// For now - each time we want to detect the language of a text - we need to instanciate a new instance of
			// a detector - this is an open issue in library we use but it is said that this does not take to much
			// resources.

			Detector langDetector = DetectorFactory.create();
			langDetector.setMaxTextLength(MAX_TEXT_LENGTH);
			langDetector.append(text);
			return langDetector.detect();
		}
		catch(Exception e)
		{
			if(Trace.debug()) Trace.debug("LanguageCatagory.findLanguageNew: Failed finding language - %s", e.getMessage());
		}

		return UNRECOGNIZED;
	}


	public static void main(String[] args) throws Exception
	{
		Configuration.initialize();
		//		String text = "hello world";
		String text = "hello world";

		//		for(char c : text.toCharArray())
		//		{
		//			Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
		//			System.out.println(block.toString());
		//		}

		//		Character.UnicodeBlock block = LanguageCategory.unicodeLanguageFinderNew(text);
		//		System.out.println(block.toString());


		String lan = LanguageCategory.findLanguage(text);
		System.out.println(lan);
	}
}
