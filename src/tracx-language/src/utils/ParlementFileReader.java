package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import tracx.common.tracing.Trace;

public class ParlementFileReader
{
	static String fileDir;
	private static FileOutputStream outputFile;

	public static void fileProcessor(String fileName) throws IOException
	{
		BufferedReader in = new BufferedReader(new FileReader(fileDir + "/" + fileName));
		StringBuilder languageRawCurpos = new StringBuilder();
		String str;
		while((str = in.readLine()) != null)
		{
			if(str.startsWith("<")) continue;
			languageRawCurpos.append(str);
			languageRawCurpos.append("\r\n");
		}

		languageRawCurpos.append("\r\n");

		outputFile.write(languageRawCurpos.toString().getBytes());
		in.close();
	}

	public static void main(String[] args)
	{
		try
		{
			if(args.length < 2)
			{
				if(Trace.fatal())
				{
					Trace.fatal("Usage error:");
					Trace.fatal("\t[Directory Name] [outputfile]");
					return;
				}
			}
			fileDir = (args[0]);
			outputFile = new FileOutputStream(new File(args[1]));

			File dir = new File(fileDir);
			String[] listOfFiles = dir.list();
			for(String fileName : listOfFiles)
			{
				fileProcessor(fileName);
			}

//			for(int i = 0 ; i < 1 ; i++)
//			{
//				fileProcessor(listOfFiles[i]);
//			}

			outputFile.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
