/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Feb 2016                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	10/02/2016	Niv				Created
//
//

package tracx.common.tracing.metrics;

public interface TracxMetricsNameTranslator
{
	/**
	 * @param name original value
	 * @return null to keep original value or metrics20 formatted name
	 */
	public String translate(String name);
}
