/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2016                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	13/01/2016	Niv				Created
//
//

package tracx.common.tracing.metrics;

import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmPoint;

public class TracxMetricsEtm
{
	public static String MUTED_SUFFIX = "$";
	public static String METERED_SUFFIX = "#";

	public static EtmPoint createMutedPoint(String name)
	{
		return EtmManager.getEtmMonitor().createPoint(name + MUTED_SUFFIX);
	}

	public static void collectMutedPoint(String name)
	{
		createMutedPoint(name).collect();
	}

	public static boolean isMuted(EtmPoint point)
	{
		return point.getName().endsWith(MUTED_SUFFIX);
	}


	/*
	 * Point used just for counting the rate of events
	 */
	public static void collectMeteredPoint(String name)
	{
		EtmManager.getEtmMonitor().createPoint(name + METERED_SUFFIX).collect();
	}

	public static boolean isMetered(EtmPoint point)
	{
		return point.getName().endsWith(METERED_SUFFIX);
	}

	public static String getMeteredPointName(EtmPoint point)
	{
		String name = point.getName();
		return isMetered(point) ? name.substring(0, name.length()-METERED_SUFFIX.length()) : name;
	}
}
