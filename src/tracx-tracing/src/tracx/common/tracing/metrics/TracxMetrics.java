/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Oct 2014                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	23/10/2014	Niv				Created
//
//

package tracx.common.tracing.metrics;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Constructor;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.MetricSet;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.Timer;
import com.codahale.metrics.jvm.BufferPoolMetricSet;
import com.codahale.metrics.jvm.CachedThreadStatesGaugeSet;
import com.codahale.metrics.jvm.ClassLoadingGaugeSet;
import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import tracx.common.tracing.Trace;
import tracx.common.tracing.metrics.Metric.MetricType;

public class TracxMetrics
{
	private static final boolean DEFAULT_ENABLE = false;											// metrics.enable
	private static final boolean DEFAULT_ENABLE_METRICS_GAUGES_ONLY = true;                        // metrics.gauges_only -- don't use Metrics for Counters, Meters, Timers (actually, use only for JVM)
	private static final String DEFAULT_STAGING = "local";											// metrics.staging
	private static final String DEFAULT_SERVICE = "%mainclass%";									// mertics.service
	private static final String DEFAULT_METRICS_PREFIX = "tracx.env=backend.staging=%staging%.server=%hostname%.instance=%dir%.service=%service%";		// metrics.prefix
	private static final boolean DEFAULT_V2 = true;													// metrics.v2 (use metrics20 naming conventions -- http://metrics20.org/spec/)
	private static final boolean DEFAULT_ENABLE_JETM_METRICS = true;                                // metrics.jetm
	private static final boolean DEFAULT_ENABLE_JVM_METRICS = true;									// metrics.jvm
	private static final boolean DEFAULT_ENABLE_STATSD_REPORTER = false;							// metrics.statsd, metrics.statsd.server (host[:port])
	private static final int DEFAULT_STATSD_PORT = 8125;
	private static final int DEFAULT_STATSD_REPORTER_SEC = 5;										// metrics.statsd.reporter_seconds
	private static final String DEFAULT_STATSD_SENDER_IMPL = null;									// metrics.statsd.sender_impl (e.g. tracx.common.tracing.metrics.TracxStatsDSenderAsync)
//	private static final boolean DEFAULT_ENABLE_GRAPHITE_REPORTER = false;							// metrics.graphite, metrics.graphite.server (host[:port])
//	private static final int DEFAULT_GRAPHITE_PORT = 2003;
//	private static final int DEFAULT_GRAPHITE_REPORTER_SEC = 10;									// metrics.graphite.reporter_seconds
	private static final int DEFAULT_LOG_REPORTER_SEC = 10;											// metrics.log_reporter_seconds

	private static boolean ms_enable;
	private static boolean ms_enableGaugesOnly;
	private static boolean ms_v2 = true;
	private static boolean ms_enableJETMMetrics = false;											// initialized in tracx.server.Configuration.initialize
	private static String ms_prefix = null;

//	private static boolean ms_initialized = false;
	private static MetricRegistry ms_metrics = null;
	private static Slf4jReporter ms_reporterLog = null;
	private static TracxStatsDSender ms_statsdSender = null;
	private static TracxStatsDGaugesReporter ms_reporterStatsD = null;
//	private static GraphiteReporter ms_reporterGraphite = null;

	// prefix (before regex class \W "not word") to translator
	private static Map<String, TracxMetricsNameTranslator> ms_translators = new ConcurrentHashMap<String, TracxMetricsNameTranslator>();


	public static boolean isEnabled()
	{
		return ms_enable;
	}

	public static boolean isJETMMetricsEnabled()
	{
		return isEnabled() && ms_enableJETMMetrics;
	}

	public static boolean isStatsDEnabled()
	{
		return ((ms_statsdSender != null) && ms_statsdSender.isEnabled());
	}
	public static void enableStatsD(boolean enable)
	{
		if(ms_statsdSender != null)
		{
			ms_statsdSender.enable(enable);
		}
	}


	public static void addNameTranslator(String prefix, TracxMetricsNameTranslator translator)
	{
		ms_translators.put(prefix, translator);
	}


//	/**
//	 * A counter is just a gauge for an AtomicLong instance. You can increment or decrement its value.
//	 */
//	public static Counter getCounter(String name)
//	{
//		MetricRegistry metrics = ms_metrics;
//		if(metrics == null) return null;
//		return metrics.counter(sanitizeName(name));
//	}

	/**
	 * A counter is just a gauge for an AtomicLong instance. You can increment or decrement its value.
	 */
	public static void incCounter(String name, long n)
	{
		if(!ms_enable) return;

		name = sanitizeName(name);

		if(!ms_enableGaugesOnly)
		{
			MetricRegistry metrics = ms_metrics;
			if(metrics != null)
			{
				Counter counter = metrics.counter(name);
				if(counter != null)
				{
					counter.inc(n);
				}
			}
		}

		TracxStatsDSender sender = ms_statsdSender;
		if(sender != null)
		{
			sender.send(name, n, MetricType.Counter);
		}
	}


//	/**
//	 * A meter measures the rate of events over time (e.g., "requests per second"). In addition to the mean rate, meters also track 1-, 5-, and 15-minute moving averages.
//	 */
//	public static Meter getMeter(String name)
//	{
//		MetricRegistry metrics = ms_metrics;
//		if(metrics == null) return null;
//		return metrics.meter(sanitizeName(name));
//	}

	/**
	 * A meter measures the rate of events over time (e.g., "requests per second"). In addition to the mean rate, meters also track 1-, 5-, and 15-minute moving averages.
	 * NOT SUPPORTED IN https://github.com/bitly/statsdaemon
	 */
	public static void markMeter(String name, long n)
	{
		if(!ms_enable) return;

		name = sanitizeName(name);

		if(!ms_enableGaugesOnly)
		{
			MetricRegistry metrics = ms_metrics;
			if(metrics != null)
			{
				Meter meter = metrics.meter(name);
				if(meter != null)
				{
					meter.mark(n);
				}
			}
		}

		TracxStatsDSender sender = ms_statsdSender;
		if(sender != null)
		{
			sender.send(name, n, MetricType.Meter);
		}
	}


//	/**
//	 * A timer metric which aggregates timing durations and provides duration statistics, plus
//	 * throughput statistics via {@link Meter}.
//	 */
//	public static Timer getTimer(String name)
//	{
//		MetricRegistry metrics = ms_metrics;
//		if(metrics == null) return null;
//		return metrics.timer(sanitizeName(name));
//	}

	/**
	 * A timer metric which aggregates timing durations and provides duration statistics, plus
	 * throughput statistics via {@link Meter}.
	 */
	public static void updateTimer(String name, long duration, TimeUnit unit)
	{
		if(!ms_enable) return;

		name = sanitizeName(name);

		if(!ms_enableGaugesOnly)
		{
			MetricRegistry metrics = ms_metrics;
			if(metrics != null)
			{
				Timer timer = metrics.timer(name);
				if(timer != null)
				{
					timer.update(duration, unit);
				}
			}
		}

		TracxStatsDSender sender = ms_statsdSender;
		if(sender != null)
		{
			sender.send(name, unit.toMillis(duration), MetricType.Timer);
		}
	}


//	/**
//	 * A histogram measures the statistical distribution of values in a stream of data. In addition to minimum, maximum, mean, etc., it also measures median, 75th, 90th, 95th, 98th, 99th, and 99.9th percentiles.
//	 */
//	public static Histogram getHistogram(String name)
//	{
//		MetricRegistry metrics = ms_metrics;
//		if(metrics == null) return null;
//		return metrics.histogram(sanitizeName(name));
//	}


	public static void shutdown()
	{
		ms_metrics = null;

		Slf4jReporter reporterLog = ms_reporterLog;
		if(reporterLog != null)
		{
			ms_reporterLog = null;
			reporterLog.stop();
		}

		TracxStatsDGaugesReporter reporterStatsD = ms_reporterStatsD;
		if(reporterStatsD != null)
		{
			ms_reporterStatsD = null;
			reporterStatsD.close();
		}

		TracxStatsDSender sender = ms_statsdSender;
		if(sender != null)
		{
			try
			{
				sender.close();
			}
			catch(IOException e)
			{
				if(Trace.error()) Trace.error("TracxStatsD close failed: %s", e, e.getMessage());
			}
			ms_statsdSender = null;
		}

//		GraphiteReporter reporterGraphite = ms_reporterGraphite;
//		if(reporterGraphite != null)
//		{
//			ms_reporterGraphite = null;
//			reporterGraphite.close();
//		}
	}

	//////////////


	private static String addPrefix(String name)
	{
		return (ms_prefix == null ? "" : ms_prefix + ".") + name;
	}

	private static String sanitizeName(String name)
	{
		String v2name = null;
		if(ms_v2)
		{
			v2name = translate(name);
		}

		if(v2name == null)
		{
			v2name = name.replace('.', '-').replace('/', '-').replace(':', '-');
			if(ms_v2)
			{
				int firstDash = v2name.indexOf('-');
				if(firstDash > -1)
				{
					v2name = "component=" + v2name.substring(0, firstDash) + ".what=" + v2name.substring(firstDash+1);
				}
				else
				{
					v2name = "what=" + v2name;
				}
			}
			else
			{
				//v2name = name;
			}
		}

		return addPrefix(v2name);
	}


	private static String translate(String name)
	{
		String[] split = name.split("\\W");
		TracxMetricsNameTranslator translator = ms_translators.get(split[0]);
		if(translator != null)
		{
			return translator.translate(name);
		}
		return null;
	}


	public static void configure(Configuration configuration)
	{
		ms_enable = configuration.getBoolean("metrics.enable", DEFAULT_ENABLE);
		ms_enableGaugesOnly = configuration.getBoolean("metrics.gauges_only", DEFAULT_ENABLE_METRICS_GAUGES_ONLY);

		String staging = configuration.getString("metrics.staging", DEFAULT_STAGING);
		String service = configuration.getString("metrics.service", DEFAULT_SERVICE);

		initializePrefix(configuration.getString("metrics.prefix", DEFAULT_METRICS_PREFIX), staging, service);

		if(ms_enable)
		{
			if(Trace.debug()) Trace.debug("Metrics: Initializing");
			ms_metrics = new MetricRegistry();

			ms_v2 = configuration.getBoolean("metrics.v2", DEFAULT_V2);
			ms_enableJETMMetrics = configuration.getBoolean("metrics.jetm", DEFAULT_ENABLE_JETM_METRICS);
			if(Trace.debug()) Trace.debug("Metrics: JETM Metrics %s", ms_enableJETMMetrics ? "enabled" : "disabled");

			boolean enableJVMMetrics = configuration.getBoolean("metrics.jvm", DEFAULT_ENABLE_JVM_METRICS);
			if(Trace.debug()) Trace.debug("Metrics: JVM Metrics %s", enableJVMMetrics ? "enabled" : "disabled");
			if(enableJVMMetrics)
			{
				registerJVMMetrics();
			}

			ms_statsdSender = null;
			if(configuration.getBoolean("metrics.statsd", DEFAULT_ENABLE_STATSD_REPORTER))
			{
				String server = configuration.getString("metrics.statsd.server", null);
				if((server == null) || server.isEmpty())
				{
					if(Trace.warn()) Trace.warn("Metrics: No statsd server configured, not initializing reporter");
				}
				else
				{
					String host = null;
					int port = DEFAULT_STATSD_PORT;

					String[] host_port = server.split(":");
					host = host_port[0];
					if(host_port.length == 2)
					{
						try
						{
							port = Integer.parseInt(host_port[1]);
						}
						catch(NumberFormatException e)
						{
							if(Trace.error()) Trace.error("Metrics: Failed parsing statsd server port from '%s', using default port %d: %s", server, port, e.getMessage());
						}
					}

					int interval = configuration.getInt("metrics.statsd.reporter_seconds", DEFAULT_STATSD_REPORTER_SEC);
					if(interval <= 0) interval = DEFAULT_STATSD_REPORTER_SEC;

					if(Trace.debug()) Trace.debug("Metrics: Initializing statsd reporter to %s:%d (every %d seconds)", host, port, interval);

					String sender_impl = configuration.getString("metrics.statsd.sender_impl", DEFAULT_STATSD_SENDER_IMPL);
					if((sender_impl != null) && !sender_impl.isEmpty())
					{
						// used for constructing async impl from tracx-utils
						try
						{
							@SuppressWarnings("unchecked")
							Class<TracxStatsDSender> statsdSenderImpl = (Class<TracxStatsDSender>)Class.forName(sender_impl);
							Constructor<TracxStatsDSender> ctor = statsdSenderImpl.getConstructor(String.class, Integer.TYPE);
							ms_statsdSender = ctor.newInstance(host, port);
							if(Trace.notice()) Trace.notice("Successfully constructed TracxStatsDSender implementation '%s'", statsdSenderImpl.getSimpleName());
						}
						catch(ClassNotFoundException e)
						{
							if(Trace.warn()) Trace.warn("ClassNotFoundException trying to construct TracxStatsDSender implementation '%s': %s", sender_impl, e.getMessage());
						}
						catch(Exception e)
						{
							if(Trace.warn()) Trace.warn("Faild constructing TracxStatsDSender implementation '%s': %s", e, sender_impl, e.getMessage());
						}
					}

					// fallback to default implementation
					if(ms_statsdSender == null)
					{
						ms_statsdSender = new TracxStatsDSenderSync(host, port);
					}

					ms_statsdSender.configure(configuration);

					try
					{
						ms_statsdSender.connect();
						ms_statsdSender.sendSync(sanitizeName("TracxMetrics-initialize"), 1, MetricType.Meter);
					}
					catch(Exception e)
					{
						if(Trace.error()) Trace.error("TracxStatsD connect to %s:%d failed: %s", e, host, port, e.getMessage());
						ms_statsdSender = null;
					}

					if(ms_statsdSender != null)
					{
						ms_reporterStatsD = TracxStatsDGaugesReporter.forRegistry(ms_metrics).build(ms_statsdSender);//host, port);
						ms_reporterStatsD.start(interval, TimeUnit.SECONDS);
					}
				}
			}
			if(ms_statsdSender == null)
			{
				if(Trace.debug()) Trace.debug("Metrics: statsd reporting disabled");
			}

//			if(configuration.getBoolean("metrics.graphite", DEFAULT_ENABLE_GRAPHITE_REPORTER))
//			{
//				String server = configuration.getString("metrics.graphite.server", null);
//				if((server == null) || server.isEmpty())
//				{
//					if(Trace.warn()) Trace.warn("Metrics: No Graphite server configured, not initializing reporter");
//				}
//				else
//				{
//					String host = null;
//					int port = DEFAULT_GRAPHITE_PORT;
//
//					String[] host_port = server.split(":");
//					host = host_port[0];
//					if(host_port.length == 2)
//					{
//						try
//						{
//							port = Integer.parseInt(host_port[1]);
//						}
//						catch(NumberFormatException e)
//						{
//							if(Trace.error()) Trace.error("Metrics: Failed parsing Graphite server port from '%s', using default port %d: %s", server, port, e.getMessage());
//						}
//					}
//
//					int interval = configuration.getInt("metrics.graphite.reporter_seconds", DEFAULT_GRAPHITE_REPORTER_SEC);
//					if(interval <= 0) interval = DEFAULT_GRAPHITE_REPORTER_SEC;
//
//					if(Trace.debug()) Trace.debug("Metrics: Initializing Graphite reporter to %s:%d (every %d seconds)", host, port, interval);
//					ms_reporterGraphite = GraphiteReporter.forRegistry(ms_metrics).build(new GraphiteUDP(host, port));
//					ms_reporterGraphite.start(interval, TimeUnit.SECONDS);
//				}
//			}
//			else
//			{
//				if(Trace.debug()) Trace.debug("Metrics: Graphite reporting disabled");
//			}

			int reporterSeconds = configuration.getInt("metrics.log_reporter_seconds", DEFAULT_LOG_REPORTER_SEC);
			if(reporterSeconds > 0)
			{
				//ConsoleReporter reporter = ConsoleReporter.forRegistry(ms_metrics).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).build();
				ms_reporterLog = Slf4jReporter.forRegistry(ms_metrics).convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).build();
				ms_reporterLog.start(reporterSeconds, TimeUnit.SECONDS);
			}

//    	    Runtime.getRuntime().addShutdownHook(new Thread()
//    	    {
//    	        @Override
//    			public void run()
//    	        {
//					Slf4jReporter logReporter = ms_logReporter;
//					if(logReporter != null)
//					{
//						ms_logReporter = null;
//						logReporter.stop();
//					}
//    	        }
//    	    });

//			ms_initialized = true;
		}
	}


	private static void registerJVMMetrics()
	{
		registerAll("JVM.BufferPool", new BufferPoolMetricSet(ManagementFactory.getPlatformMBeanServer()));	// java7
		registerAll("JVM.ThreadStates", new CachedThreadStatesGaugeSet(10, TimeUnit.SECONDS));	//ThreadStatesGaugeSet
		registerAll("JVM.ClassLoading", new ClassLoadingGaugeSet());
		registerAll("JVM.GarbageCollector", new GarbageCollectorMetricSet());
		registerAll("JVM.MemoryUsage", new MemoryUsageGaugeSet());
		register("JVM.FileDescriptorRatio", new FileDescriptorRatioGauge());
	}

	private static void register(String name, Metric metric)
	{
		if(ms_metrics == null) return;
		ms_metrics.register(sanitizeName(name), metric);
	}

	private static void registerAll(String metricSetPrefix, MetricSet metricSet)
	{
		if(ms_metrics == null) return;

	    for(Map.Entry<String, Metric> entry : metricSet.getMetrics().entrySet())
	    {
	    	String name = metricSetPrefix + "." + entry.getKey();
	        if(entry.getValue() instanceof MetricSet)
	        {
	            registerAll(name, (MetricSet)entry.getValue());
	        }
	        else
	        {
	        	register(name, entry.getValue());
	        }
	    }
	}


	private static void initializePrefix(String pattern, String staging, String service)
	{
		// %service%        - metrics.service (allows using the variables below)
		// %staging%        - metrics.staging
		// %hostname%       - host part of localhost name
		// %hostname_full%  - full hostname
		// %dir%            - last part of current directory
		// %dir_full%       - current directory
		// %mainclass%      - last part of main class name
		// %mainclass_full% - main class name

		if((pattern == null) || pattern.isEmpty())
		{
			pattern = DEFAULT_METRICS_PREFIX;
		}

		if(pattern.indexOf("%service%") != -1)
		{
			pattern = pattern.replace("%service%", service);
		}

		String prefix = pattern;

		if(pattern.indexOf("%staging%") != -1)
		{
			prefix = prefix.replace("%staging%", staging);
		}

		if((pattern.indexOf("%hostname%") != -1) || (pattern.indexOf("%hostname_full%") != -1))
		{
			String hostname = "[unknown host]";
			try
			{
				InetAddress addr = InetAddress.getLocalHost();
				hostname = addr.getHostName();
			}
			catch(UnknownHostException e)
			{
				//if(Trace.error()) Trace.error("TracxMetrics.initializePrefix: UnknownHostException: %s", e, e.getMessage());
				hostname = "[UnknownHostException]";
			}

			if(pattern.indexOf("%hostname%") != -1)
			{
				if(hostname.indexOf('.') != -1)
				{
					hostname = hostname.substring(0, hostname.indexOf('.'));
				}
				prefix = prefix.replace("%hostname%", hostname);
			}
			else
			{
				prefix = prefix.replace("%hostname_full%", hostname);
			}
		}

		if((pattern.indexOf("%dir%") != -1) || (pattern.indexOf("%dir_full%") != -1))
		{
			String currentDirectory = "[current dir]";
			try
			{
				File dir = new File(".");
				currentDirectory = dir.getCanonicalPath();
				currentDirectory = currentDirectory.replace('\\', '/');
			}
			catch(Exception e)
			{
				//if(Trace.error()) Trace.error("TracxMetrics.initializePrefix: Couldn't get current directory: %s", e.getMessage());
				currentDirectory = "[current dir error]";
			}

			int lastSlash = currentDirectory.lastIndexOf('/');
			if(pattern.indexOf("%dir%") != -1)
			{
				if(lastSlash != -1)
				{
					currentDirectory = currentDirectory.substring(lastSlash+1);
				}
			}

			currentDirectory = currentDirectory.replace('/', '-').replace(':', '-').replace('.', '_');

			if(pattern.indexOf("%dir%") != -1)
			{
				prefix = prefix.replace("%dir%", currentDirectory);
			}
			else
			{
				prefix = prefix.replace("%dir_full%", currentDirectory);
			}
		}

		if((pattern.indexOf("%mainclass%") != -1) || (pattern.indexOf("%mainclass_full%") != -1))
		{
			String mainclass;

			mainclass = System.getProperty("sun.java.command");

			if(mainclass == null)
			{
				for(final Map.Entry<String, String> entry : System.getenv().entrySet())
				{
					if(entry.getKey().startsWith("JAVA_MAIN_CLASS"))
					{
						mainclass = entry.getValue();
						//System.out.println("java_main_class = " + mainclass);
						break;
					}
				}
			}

			if(mainclass == null)
			{
				Map<Thread, StackTraceElement[]> stackTraceMap = Thread.getAllStackTraces();
				for(Thread t : stackTraceMap.keySet())
				{
					if("main".equals(t.getName()))
					{
						StackTraceElement[] stackTrace = stackTraceMap.get(t);
						mainclass = stackTrace[stackTrace.length-1].getClassName();
						//System.out.println("all stack traces = " + mainclass);
						break;
					}
				}
			}

			if(mainclass == null)
			{
				StackTraceElement[] stackTrace = new Throwable().getStackTrace();
				mainclass = stackTrace[stackTrace.length-1].getClassName();
				//System.out.println("this stacktrace = " + mainclass);
			}

			if(mainclass == null)
			{
				mainclass = "[main]";
			}

			if(pattern.indexOf("%mainclass%") != -1)
			{
				int lastDot = mainclass.lastIndexOf('.');
				if(lastDot != -1)
				{
					mainclass = mainclass.substring(lastDot+1);
				}
				prefix = prefix.replace("%mainclass%", mainclass);
			}
			else
			{
				mainclass = mainclass.replace('.', '-');
				prefix = prefix.replace("%mainclass_full%", mainclass);
			}
		}

		ms_prefix = prefix;
	}



	/////////////////////////////////////

	public static void main(String[] args)
	{
		//configure(null);
    	try
		{
    		PropertiesConfiguration configuration = new PropertiesConfiguration();
    		configuration.setDelimiterParsingDisabled(true);
			configuration.load("../config/tra.cx.server.properties");
			Trace.initialize(configuration);
		}
		catch(ConfigurationException e1)
		{
			throw new IllegalStateException(e1);
		}

    	if(ms_metrics == null)
    	{
    		System.out.println("metrics is disabled");
    		return;
    	}

		//Meter requests = getMeter("requests");
		//Counter counter = getCounter("counter");
		//Histogram sleeps = getHistogram("sleeps");

		while(true)
		{
			markMeter("requests", 1); //requests.mark();
			incCounter("counter", 1); //counter.inc();

			int sleep = (new Random()).nextInt(1000);
			//sleeps.update(sleep);
			try
			{
				Thread.sleep(sleep);
				updateTimer("sleeps", sleep, TimeUnit.MILLISECONDS);
			}
			catch(InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
