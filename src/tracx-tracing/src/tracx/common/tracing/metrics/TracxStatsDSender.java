/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	20/01/2015	Niv				Created
//
//

package tracx.common.tracing.metrics;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import org.apache.commons.configuration.Configuration;
import tracx.common.tracing.metrics.Metric.MetricType;

public interface TracxStatsDSender extends Closeable
{
	// specify a constructor TracxStatsDSender(final String host, final int port)

	public void configure(Configuration configuration);

	public boolean isEnabled();
	public void enable(boolean enable);

	public void connect() throws IOException;
	public void close() throws IOException;

	public void send(final String name, final Number value, MetricType type);
	public void send(Metric metric);
	public void send(Collection<Metric> metrics);

	public void sendSync(final String name, final Number value, MetricType type);
	public void sendSync(Metric metric);
	public void sendSync(Collection<Metric> metrics);
}
