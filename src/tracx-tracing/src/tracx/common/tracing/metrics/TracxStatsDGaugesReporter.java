/**
/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	20/01/2015	Niv				Created
//
//

 * Based on
 *
 * Copyright (C) 2013 metrics-statsd contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *	 you may not use this file except in compliance with the License.
 *	 You may obtain a copy of the License at
 *
 *			 http://www.apache.org/licenses/LICENSE-2.0
 *
 *	 Unless required by applicable law or agreed to in writing, software
 *	 distributed under the License is distributed on an "AS IS" BASIS,
 *	 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *	 See the License for the specific language governing permissions and
 *	 limitations under the License.
 */
package tracx.common.tracing.metrics;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Timer;
import tracx.common.tracing.metrics.Metric.MetricType;

/**
 * A reporter which publishes metric values to a StatsD server.
 *
 * @see <a href="https://github.com/etsy/statsd">StatsD</a>
 */
public class TracxStatsDGaugesReporter extends ScheduledReporter
{
	private final TracxStatsDSender statsD;
	private final String prefix;

//	private class Delta
//	{
//		private MetricType m_type;
//		private String m_name;
//		private AtomicLong m_delta = new AtomicLong();
//
//		public Delta(MetricType type, String name)
//		{
//		}
//	}

	private TracxStatsDGaugesReporter(final MetricRegistry registry,
							 final TracxStatsDSender statsD,
							 final String prefix,
							 final TimeUnit rateUnit,
							 final TimeUnit durationUnit,
							 final MetricFilter filter)
	{
		super(registry, "tracx-statsd-reporter", filter, rateUnit, durationUnit);
		this.statsD = statsD;
		this.prefix = prefix;
	}

	/**
	 * Returns a new {@link Builder} for {@link TracxStatsDGaugesReporter}.
	 *
	 * @param registry the registry to report
	 * @return a {@link Builder} instance for a {@link TracxStatsDGaugesReporter}
	 */
	public static Builder forRegistry(final MetricRegistry registry)
	{
		return new Builder(registry);
	}

	/**
	 * A builder for {@link TracxStatsDGaugesReporter} instances. Defaults to not using a prefix,
	 * converting rates to events/second, converting durations to milliseconds, and not
	 * filtering metrics.
	 */
	public static final class Builder
	{
		private final MetricRegistry registry;
		private String prefix;
		private TimeUnit rateUnit;
		private TimeUnit durationUnit;
		private MetricFilter filter;

		private Builder(final MetricRegistry registry)
		{
			this.registry = registry;
			prefix = null;
			rateUnit = TimeUnit.SECONDS;
			durationUnit = TimeUnit.MILLISECONDS;
			filter = MetricFilter.ALL;
		}

		/**
		 * Prefix all metric names with the given string.
		 *
		 * @param _prefix the prefix for all metric names
		 * @return {@code this}
		 */
		public Builder prefixedWith(final String _prefix)
		{
			prefix = _prefix;
			return this;
		}

		/**
		 * Convert rates to the given time unit.
		 *
		 * @param _rateUnit a unit of time
		 * @return {@code this}
		 */
		public Builder convertRatesTo(final TimeUnit _rateUnit)
		{
			rateUnit = _rateUnit;
			return this;
		}

		/**
		 * Convert durations to the given time unit.
		 *
		 * @param _durationUnit a unit of time
		 * @return {@code this}
		 */
		public Builder convertDurationsTo(final TimeUnit _durationUnit)
		{
			durationUnit = _durationUnit;
			return this;
		}

		/**
		 * Only report metrics which match the given filter.
		 *
		 * @param _filter a {@link MetricFilter}
		 * @return {@code this}
		 */
		public Builder filter(final MetricFilter _filter)
		{
			filter = _filter;
			return this;
		}

//		/**
//		 * Builds a {@link TracxStatsDGaugesReporter} with the given properties, sending metrics to StatsD at the given host and port.
//		 *
//		 * @param host the hostname of the StatsD server.
//		 * @param port the port of the StatsD server. This is typically 8125.
//		 * @return a {@link TracxStatsDGaugesReporter}
//		 */
//		public TracxStatsDGaugesReporter build(final String host, final int port)
//		{
//			return build(new TracxStatsDSender(host, port));
//		}

		/**
		 * Builds a {@link TracxStatsDGaugesReporter} with the given properties, sending metrics using the
		 * given {@link TracxStatsDSender} client.
		 *
		 * @param statsD a {@link TracxStatsDSender} client
		 * @return a {@link TracxStatsDGaugesReporter}
		 */
		public TracxStatsDGaugesReporter build(final TracxStatsDSender statsD)
		{
			return new TracxStatsDGaugesReporter(registry, statsD, prefix, rateUnit, durationUnit, filter);
		}
	}


	@Override
	@SuppressWarnings("rawtypes") //Metrics 3.0 interface specifies the raw Gauge type
	public void report(final SortedMap<String, Gauge> gauges,
										 final SortedMap<String, Counter> counters,
										 final SortedMap<String, Histogram> histograms,
										 final SortedMap<String, Meter> meters,
										 final SortedMap<String, Timer> timers)
	{
//		try
//		{
//			statsD.connect();

			Collection<Metric> metrics = new ArrayList<Metric>(gauges.size());

			if((gauges != null) && !gauges.isEmpty())
			{
				for(Map.Entry<String, Gauge> entry : gauges.entrySet())
				{
					Metric metric = reportGauge(entry.getKey(), entry.getValue());
					if(metric != null)
					{
						metrics.add(metric);
					}
				}

				statsD.send(metrics);
			}

//			for(Map.Entry<String, Counter> entry : counters.entrySet())
//			{
//				reportCounter(entry.getKey(), entry.getValue());
//			}
//
//			for(Map.Entry<String, Histogram> entry : histograms.entrySet())
//			{
//				reportHistogram(entry.getKey(), entry.getValue());
//			}
//
//			for(Map.Entry<String, Meter> entry : meters.entrySet())
//			{
//				reportMetered(entry.getKey(), entry.getValue());
//			}
//
//			for(Map.Entry<String, Timer> entry : timers.entrySet())
//			{
//				reportTimer(entry.getKey(), entry.getValue());
//			}
//		}
//		catch(IOException e)
//		{
//			if(Trace.warn()) Trace.warn("Unable to report to StatsD", e);
//		}
//		finally
//		{
//			try
//			{
//				statsD.close();
//			}
//			catch(IOException e)
//			{
//				if(Trace.debug()) Trace.debug("Error disconnecting from StatsD", e);
//			}
//		}
	}

//	private void reportTimer(final String name, final Timer timer)
//	{
//		if(Trace.debug()) Trace.debug("Not reporting Timer %s", name);
//
////		final Snapshot snapshot = timer.getSnapshot();
////
////		statsD.send(prefix(name, "max"), formatNumber(convertDuration(snapshot.getMax())));
////		statsD.send(prefix(name, "mean"), formatNumber(convertDuration(snapshot.getMean())));
////		statsD.send(prefix(name, "min"), formatNumber(convertDuration(snapshot.getMin())));
////		statsD.send(prefix(name, "stddev"), formatNumber(convertDuration(snapshot.getStdDev())));
////		statsD.send(prefix(name, "p50"), formatNumber(convertDuration(snapshot.getMedian())));
////		statsD.send(prefix(name, "p75"), formatNumber(convertDuration(snapshot.get75thPercentile())));
////		statsD.send(prefix(name, "p95"), formatNumber(convertDuration(snapshot.get95thPercentile())));
////		statsD.send(prefix(name, "p98"), formatNumber(convertDuration(snapshot.get98thPercentile())));
////		statsD.send(prefix(name, "p99"), formatNumber(convertDuration(snapshot.get99thPercentile())));
////		statsD.send(prefix(name, "p999"), formatNumber(convertDuration(snapshot.get999thPercentile())));
////
////		reportMetered(name, timer);
//	}
//
//	private void reportMetered(final String name, final Metered meter)
//	{
//		if(Trace.debug()) Trace.debug("Not reporting Metered %s, count=%d", name, meter.getCount());
//
////		statsD.send(prefix(name, "samples"), formatNumber(meter.getCount()));
////		statsD.send(prefix(name, "m1_rate"), formatNumber(convertRate(meter.getOneMinuteRate())));
////		statsD.send(prefix(name, "m5_rate"), formatNumber(convertRate(meter.getFiveMinuteRate())));
////		statsD.send(prefix(name, "m15_rate"), formatNumber(convertRate(meter.getFifteenMinuteRate())));
////		statsD.send(prefix(name, "mean_rate"), formatNumber(convertRate(meter.getMeanRate())));
//	}
//
//	private void reportHistogram(final String name, final Histogram histogram)
//	{
//		if(Trace.debug()) Trace.debug("Not reporting Histogram %s", name);
//
////		final Snapshot snapshot = histogram.getSnapshot();
////		statsD.send(prefix(name, "samples"), formatNumber(histogram.getCount()));
////		statsD.send(prefix(name, "max"), formatNumber(snapshot.getMax()));
////		statsD.send(prefix(name, "mean"), formatNumber(snapshot.getMean()));
////		statsD.send(prefix(name, "min"), formatNumber(snapshot.getMin()));
////		statsD.send(prefix(name, "stddev"), formatNumber(snapshot.getStdDev()));
////		statsD.send(prefix(name, "p50"), formatNumber(snapshot.getMedian()));
////		statsD.send(prefix(name, "p75"), formatNumber(snapshot.get75thPercentile()));
////		statsD.send(prefix(name, "p95"), formatNumber(snapshot.get95thPercentile()));
////		statsD.send(prefix(name, "p98"), formatNumber(snapshot.get98thPercentile()));
////		statsD.send(prefix(name, "p99"), formatNumber(snapshot.get99thPercentile()));
////		statsD.send(prefix(name, "p999"), formatNumber(snapshot.get999thPercentile()));
//	}
//
//	private void reportCounter(final String name, final Counter counter)
//	{
//		if(Trace.debug()) Trace.debug("Not reporting Counter %s, count=%d", name, counter.getCount());
////		statsD.send(prefix(name), formatNumber(counter.getCount()));
//	}

	@SuppressWarnings("rawtypes") //Metrics 3.0 passes us the raw Gauge type
	private Metric reportGauge(final String name, final Gauge gauge)
	{
		//final String value = format(gauge.getValue());
		Number value = toNumber(gauge.getValue());
		if(value != null)
		{
			//statsD.send(prefix(name), value, MetricType.Gauge);
			return TracxStatsDSenderSync.getMetric(prefix(name), value, MetricType.Gauge);
		}

		return null;
	}

	private String prefix(final String... components)
	{
		return MetricRegistry.name(prefix, components);
	}

	private Number toNumber(final Object o)
	{
		if(o instanceof Float)
		{
			return ((Float)o).doubleValue();
		}
		else if(o instanceof Double)
		{
			return (Double)o;
		}
		else if(o instanceof Byte)
		{
			return ((Byte)o).longValue();
		}
		else if(o instanceof Short)
		{
			return ((Short)o).longValue();
		}
		else if(o instanceof Integer)
		{
			return ((Integer)o).longValue();
		}
		else if(o instanceof Long)
		{
			return (Long)o;
		}
		else if(o instanceof BigInteger)
		{
			return (BigInteger)o;
		}
		else if(o instanceof BigDecimal)
		{
			return ((BigDecimal)o).doubleValue();
		}

		return null;
	}


//	private String format(final Object o)
//	{
//		if(o instanceof Float)
//		{
//			return formatNumber(((Float)o).doubleValue());
//		}
//		else if(o instanceof Double)
//		{
//			return formatNumber((Double)o);
//		}
//		else if(o instanceof Byte)
//		{
//			return formatNumber(((Byte)o).longValue());
//		}
//		else if(o instanceof Short)
//		{
//			return formatNumber(((Short)o).longValue());
//		}
//		else if(o instanceof Integer)
//		{
//			return formatNumber(((Integer)o).longValue());
//		}
//		else if(o instanceof Long)
//		{
//			return formatNumber((Long)o);
//		}
//		else if(o instanceof BigInteger)
//		{
//			return formatNumber((BigInteger)o);
//		}
//		else if(o instanceof BigDecimal)
//		{
//			return formatNumber(((BigDecimal)o).doubleValue());
//		}
//
//		return null;
//	}
//
//	private String formatNumber(final BigInteger n)
//	{
//		return String.valueOf(n);
//	}
//
//	private String formatNumber(final long n)
//	{
//		return Long.toString(n);
//	}
//
//	private String formatNumber(final double v)
//	{
//		return String.format(Locale.US, "%2.2f", v);
//	}
}
