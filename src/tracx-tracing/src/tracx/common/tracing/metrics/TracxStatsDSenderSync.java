/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	20/01/2015	Niv				Created
//
//

/**
 * Based on
 *
 * Copyright (C) 2013 metrics-statsd contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package tracx.common.tracing.metrics;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import org.apache.commons.configuration.Configuration;
import tracx.common.tracing.Trace;
import tracx.common.tracing.metrics.Metric.MetricType;

/**
 * A client to a StatsD server.
 */
public class TracxStatsDSenderSync implements TracxStatsDSender
{
	protected static final int DEFAULT_STATSD_MTU = 1500;	// metrics.statsd.mtu
	protected static final boolean DEFAULT_METER_AS_COUNTER = true; // metrics.statsd.meter_as_counter

	protected static final Pattern WHITESPACE = Pattern.compile("[\\s]+");
	protected static final Charset UTF_8 = Charset.forName("UTF-8");

	protected final DatagramSocketFactory m_socketFactory;

	protected InetSocketAddress m_address;
	protected boolean m_enabled = true;
	protected int m_maxPayloadLength = 0;
	protected boolean m_meterAsCounter = DEFAULT_METER_AS_COUNTER;
	protected DatagramSocket m_socket;
	protected AtomicInteger m_failures = new AtomicInteger();

	/**
	 * Creates a new client which connects to the given address using the default {@link DatagramSocketFactory}.
	 *
	 * @param host the hostname of the StatsD server.
	 * @param port the port of the StatsD server. This is typically 8125.
	 */
	public TracxStatsDSenderSync(final String host, final int port)
	{
		this(new InetSocketAddress(host, port), new DatagramSocketFactory());
	}

	/**
     * Creates a new client which connects to the given address and socket factory.
     *
     * @param address       the address of the Carbon server
     * @param socketFactory the socket factory
     */
	public TracxStatsDSenderSync(final InetSocketAddress address, final DatagramSocketFactory socketFactory)
	{
		m_address = address;
		m_socketFactory = socketFactory;
	}

	public void configure(Configuration configuration)
	{
		int mtu = configuration.getInt("metrics.statsd.mtu", DEFAULT_STATSD_MTU);
		if(mtu == 0)
		{
			m_maxPayloadLength = 0;
		}
		else
		{
			int maxPayloadLength = (mtu - m_socketFactory.getPacketHeaderLength());
			m_maxPayloadLength = Math.max(0, maxPayloadLength);
		}

		// https://github.com/bitly/statsdaemon doesn't support Meters
		m_meterAsCounter = configuration.getBoolean("metrics.statsd.statsdaemon_meter_as_counter", DEFAULT_METER_AS_COUNTER);

//		// auto detect relevant network interface's mtu
//		// see possible impl in http://stackoverflow.com/a/8462548/1442259
//		try
//		{
//			NetworkInterface iface = NetworkInterface.getByInetAddress(m_address.getAddress());
//			mtu = iface.getMTU();
//			if(Trace.debug()) Trace.debug("Detected Network Interface '%s' MTU to %s as %d", iface.getDisplayName(), m_address, mtu);
//		}
//		catch(Exception e)
//		{
//			if(Trace.error()) Trace.error("Failed detecting Network Interface's MTU to %s: %s", m_address, e.getMessage());
//		}
	}

	@Override
	public boolean isEnabled()
	{
		return m_enabled;
	}

	@Override
	public void enable(boolean enable)
	{
		m_enabled = enable;
	}


	/**
	 * Resolves the address hostname if present.
	 * <p/>
	 * Creates a datagram socket through the factory.
	 *
	 * @throws IllegalStateException if the client is already connected
	 * @throws IOException           if there is an error connecting
	 */
	public void connect() throws IOException
	{
		if(m_socket != null)
		{
			throw new IllegalStateException("Already connected");
		}

		if(m_address.getHostName() != null)
		{
			m_address = new InetSocketAddress(m_address.getHostName(), m_address.getPort());
		}

		m_socket = m_socketFactory.createSocket();
	}

	public void close() throws IOException
  	{
		DatagramSocket socket = m_socket;
		if(socket != null)
		{
			m_socket = null;
			socket.close();
		}
  	}


	public static Metric getMetric(final String name, final Number value, MetricType type)
	{
		return new Metric(name, value, type);
	}

	public String formatMetric(Metric metric)
	{
		String value = format(metric.value);
		if(value == null)
		{
			if(Trace.verbose()) Trace.verbose("Failed converting %s value %s", metric.name, String.valueOf(metric.value));
			return null;
		}

		return String.format("%s:%s|%s", sanitize(metric.name), value, getMetricTypeStr(metric.type));
	}

	private String getMetricTypeStr(MetricType type)
	{
		if(m_meterAsCounter && (type == MetricType.Meter))
		{
			// https://github.com/bitly/statsdaemon doesn't support Meters
			return getMetricTypeStr(MetricType.Counter);
		}
		return type.getTypeStr();
	}


	public void send(final String name, final Number value, MetricType type)
	{
		send(getMetric(name, value, type));
	}


	public void send(Metric metric)
	{
		send(Arrays.asList(metric));
	}

	public void send(Collection<Metric> metrics)
	{
		reallySend(metrics);
	}


	public final void sendSync(final String name, final Number value, MetricType type)
	{
		sendSync(getMetric(name, value, type));
	}

	public void sendSync(Metric metric)
	{
		sendSync(Arrays.asList(metric));
	}

	public void sendSync(Collection<Metric> metrics)
	{
		reallySend(metrics);
	}


	/**
	 * @return number of packets
	 */
	protected int reallySend(Collection<Metric> metrics)
	{
		if(!isEnabled()) return 0;
		if((metrics == null) || metrics.isEmpty()) return 0;

		int packets = 0;

		if((metrics.size() == 1) || (m_maxPayloadLength == 0))
		{
			for(Metric metric : metrics)
			{
				String formatted = formatMetric(metric);
				if(formatted == null) continue;

				packets++;
				reallySend(formatted);
			}
		}
		else
		{
			// combine multiple metrics into a single packet
			StringBuilder sb = new StringBuilder();
			boolean first = true;
			for(Metric metric : metrics)
			{
				String formatted = formatMetric(metric);
				if(formatted == null) continue;

				int len = formatted.length() + (first ? 0 : 1);
				if((sb.length() + len) > m_maxPayloadLength)
				{
					if(first)
					{
						// first item is bigger than MTU, send it by itself
						packets++;
						reallySend(formatted);
					}
					else
					{
						// this item won't fit, send buffer, clear it and append the item
						packets++;
						reallySend(sb.toString());
						sb.setLength(0);
						sb.append(formatted);
					}
				}
				else
				{
					if(first)
					{
						first = false;
					}
					else
					{
						sb.append('\n');
					}
					sb.append(formatted);
				}
			}

			if(sb.length() > 0)
			{
				packets++;
				reallySend(sb.toString());
			}
		}

		return packets;
	}

	protected void reallySend(String payload)
	{
		byte[] bytes = payload.getBytes(UTF_8);
		reallySend(bytes);
	}

	/**
	 * Sends the given measurement to the server. Logs exceptions.
	 */
	protected void reallySend(byte[] bytes)
	{
		try
		{
			DatagramSocket socket = m_socket;
			if(socket == null)
			{
				connect();
			}

			m_socket.send(m_socketFactory.createPacket(bytes, bytes.length, m_address));
			m_failures.set(0);
		}
		catch(IOException e)
		{
			int failures = m_failures.incrementAndGet();
			if(failures == 1)
			{
				if(Trace.error()) Trace.error("unable to send packet to statsd at %s:%d", m_address.getHostName(), m_address.getPort());
			}
			else
			{
				if(Trace.lowlevel()) Trace.error("unable to send packet to statsd at %s:%d", m_address.getHostName(), m_address.getPort());
			}
		}
	}

	/**
	 * Returns the number of failed writes to the server.
	 *
	 * @return the number of failed writes to the server
	 */
	public int getFailures()
	{
		return m_failures.get();
	}


	protected static String sanitize(final String s)
	{
		return WHITESPACE.matcher(s).replaceAll("-");
	}

	protected static String format(final Number o)
	{
		if(o instanceof Float)
		{
			return formatNumber(((Float)o).doubleValue());
		}
		else if(o instanceof Double)
		{
			return formatNumber((Double)o);
		}
		else if(o instanceof Byte)
		{
			return formatNumber(((Byte)o).longValue());
		}
		else if(o instanceof Short)
		{
			return formatNumber(((Short)o).longValue());
		}
		else if(o instanceof Integer)
		{
			return formatNumber(((Integer)o).longValue());
		}
		else if(o instanceof Long)
		{
			return formatNumber((Long)o);
		}
		else if(o instanceof BigInteger)
		{
			return formatNumber((BigInteger)o);
		}
		else if(o instanceof BigDecimal)
		{
			return formatNumber(((BigDecimal)o).doubleValue());
		}

		return null;
	}

	protected static String formatNumber(final BigInteger n)
	{
		return String.valueOf(n);
	}

	protected static String formatNumber(final long n)
	{
		return Long.toString(n);
	}

	protected static String formatNumber(final double v)
	{
		if(Double.isNaN(v)) return null;
		return String.format(Locale.US, "%2.2f", v);
	}


  	public static class DatagramSocketFactory
  	{
  		public DatagramSocket createSocket() throws SocketException
  		{
  			return new DatagramSocket();
  		}

  		public DatagramPacket createPacket(final byte[] bytes, final int length, final InetSocketAddress address) throws SocketException
  		{
  			return new DatagramPacket(bytes, length, address);
  		}

  		public int getPacketHeaderLength()
  		{
  			return 8;
  		}
	}
}
