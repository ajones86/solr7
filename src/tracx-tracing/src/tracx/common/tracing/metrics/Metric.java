/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2015 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Feb 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	10/02/2015	Niv				Created
//
//

package tracx.common.tracing.metrics;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Metric
{
	// See https://github.com/b/statsd_spec
	public enum MetricType
	{
		Gauge("g"),
		Counter("c"),
		Timer("ms"),
		Histogram("h"),
		Meter("m");

		private String m_type;

		MetricType(String type)
		{
			m_type = type;
		}

		public String getTypeStr()
		{
			return m_type;
		}
	}


	public String name;
	public Number value;
	public MetricType type;

	public Metric(String name, Number value, MetricType type)
	{
		if(name == null) throw new IllegalArgumentException("name is null");
		if(value == null) throw new IllegalArgumentException("value is null");
		if(type == null) throw new IllegalArgumentException("type is null");

		this.name = name;
		this.value = value;
		this.type = type;
	}

	public void aggregate(Metric m)
	{
		// a little help from http://stackoverflow.com/a/3851004/1442259

		if(!type.equals(m.type))
		{
			throw new IllegalArgumentException(String.format("Metric.aggregate: Incompatible Metric types (first=%s, second=%s)", type, m.type));
		}
		else if(!type.equals(MetricType.Counter) && !type.equals(MetricType.Meter))
		{
			throw new IllegalArgumentException(String.format("Metric.aggregate: Can only aggregate Counters and Meters (type=%s)", type));
		}

		Number first = value;
		Number second = m.value;

        if((first instanceof BigDecimal) || (first instanceof BigInteger) || (second instanceof BigDecimal) || (second instanceof BigInteger))
        {
			if(!first.getClass().equals(second.getClass()))
			{
				throw new IllegalArgumentException(String.format("Metric.aggregate: Incompatible value types (first=%s, second=%s)", first.getClass().getSimpleName(), second.getClass().getSimpleName()));
			}

	        if(first instanceof BigDecimal)
	        {
	        	value = ((BigDecimal)first).add((BigDecimal)second);
	        }
	        else if(first instanceof BigInteger)
	        {
	        	value = ((BigInteger)first).add((BigInteger)second);
	        }
        }
        else if(((first instanceof Double) || (first instanceof Float)) && ((second instanceof Double) || (second instanceof Float)))
        {
        	// always store Double
        	value = first.doubleValue() + second.doubleValue();
        }
        else if((second instanceof Byte) || (second instanceof Integer) || (second instanceof Long) || (second instanceof Short))
        {
        	// always store Long
        	value = first.longValue() + second.longValue();
        }
        else
        {
        	throw new UnsupportedOperationException("Metric.aggregate: Unsupported Number " + second.getClass().getSimpleName());
        }
	}
}