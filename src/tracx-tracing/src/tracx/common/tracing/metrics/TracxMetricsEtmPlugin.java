/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2016 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	13/01/2015	Niv				Created
//
//

package tracx.common.tracing.metrics;

import java.util.concurrent.TimeUnit;
import etm.core.aggregation.NotifyingAggregator;
import etm.core.metadata.AggregatorMetaData;
import etm.core.metadata.PluginMetaData;
import etm.core.monitor.EtmException;
import etm.core.monitor.EtmMonitorContext;
import etm.core.monitor.EtmPoint;
import etm.core.monitor.event.CollectEvent;
import etm.core.monitor.event.CollectionListener;
import etm.core.plugin.EtmPlugin;

public class TracxMetricsEtmPlugin implements EtmPlugin, CollectionListener
{
	private final static String PREFIX = "";//"JETM.";

	protected EtmMonitorContext ctx;

	@Override
	public void init(EtmMonitorContext ctx)
	{
	    this.ctx = ctx;
	}

	@Override
	public void start()
	{
	    // validate we have a NotifyingAggregator in the chain
	    doValidateChain();
	}

	@SuppressWarnings("unchecked")
	protected void doValidateChain()
	{
		// via etm.contrib.rrd.core.AbstractRrdPlugin
		AggregatorMetaData metaData = ctx.getEtmMonitor().getMetaData().getAggregatorMetaData();
		while(metaData != null)
		{
			if(metaData.getImplementationClass().isAssignableFrom(NotifyingAggregator.class))
			{
				return;
			}
			metaData = metaData.getNestedMetaData();
		}

		throw new EtmException("Missing NotifyingAggregator. There has to be a NotifyingAggregator in your aggregation chain. Metrics support disabled.");
	}

	@Override
	public void onCollect(CollectEvent event)
	{
		EtmPoint point = event.getPoint();
		boolean muted = TracxMetricsEtm.isMuted(point);
		boolean metered = TracxMetricsEtm.isMetered(point);

		if(!muted)
		{
			if(metered)
			{
				String name = TracxMetricsEtm.getMeteredPointName(point);
				TracxMetrics.markMeter(PREFIX + name, 1);
			}
			else
			{
				//TracxMetrics.update(TracxMetrics.getTimer(PREFIX + point.getName()), (long)point.getTransactionTime(), TimeUnit.MILLISECONDS);
				TracxMetrics.updateTimer(PREFIX + point.getName(), (long)point.getTransactionTime(), TimeUnit.MILLISECONDS);
			}
		}
	}

	@Override
	public void stop()
	{
	}

	@Override
	public PluginMetaData getPluginMetaData()
	{
	    PluginMetaData metaData = new PluginMetaData(getClass(), "JETM Metrics Plugin");
	    return metaData;
	}
}
