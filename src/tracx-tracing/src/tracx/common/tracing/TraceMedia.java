/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2008 Tra.cx, All rights reserved                //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	01/12/2007	Niv				Created
//
//

package tracx.common.tracing;

public enum TraceMedia
{
	Console ("STDOUT"),
	File ("LogFile");
	
	private String m_appenderName;
	
	TraceMedia(String appenderName)
	{
		m_appenderName = appenderName;
	}
	
	public String getAppenderName()
	{
		return m_appenderName;
	}
}
