/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2008 Tra.cx, All rights reserved                //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	01/12/2007	Niv				Created
//
//

package tracx.common.tracing;

import org.apache.logging.log4j.Level;

//@SuppressWarnings("serial")
class CustomLevel
{
	public static final int VERBOSE_INT  	= Level.TRACE.intLevel() + 200; // 800
	public static final int LOWLEVEL_INT 	= Level.TRACE.intLevel() + 100; // 700
	public static final int NOTICELEVEL_INT = Level.WARN.intLevel() + 50; // 350
	
	/**
	 * This has to match the log4j2.xml "<CustomLevels>" !!!
	 */
	public static final Level VERBOSE  = Level.forName("VERBO", VERBOSE_INT);
	public static final Level LOWLEVEL = Level.forName("LOW", LOWLEVEL_INT);
	public static final Level NOTICE   = Level.forName("NOTIC", NOTICELEVEL_INT);
}
