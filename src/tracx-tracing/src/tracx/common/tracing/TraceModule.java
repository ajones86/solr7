/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2012 Tra.cx, All rights reserved                //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	01/02/2008	Niv				Added valueOfIgnoreCase
//	27/01/2007	Niv				Added JCS
//	06/12/2007	Niv				Created
//
//

package tracx.common.tracing;

public enum TraceModule
{
	// com.ibatis.common.jdbc.SimpleDataSource
	//
	// java.sql.Connection
	// java.sql.PreparedStatement
	// java.sql.ResultSet
	//
	// org.apache.jcs.config.OptionConverter
	// org.apache.jcs.utils.threadpool.ThreadPoolManager
	// org.apache.jcs.engine.control.CompositeCacheManager
	// org.apache.jcs.engine.control.CompositeCacheConfigurator
	
	
	/**
	 * This has to match the log4j2.xml "<Loggers>" !!!
	 */
    MyBATIS("org.apache.ibatis"),
	SQL("java.sql"),
	JCS("org.apache.jcs"),
	Hadoop("org.apache.hadoop"),
	Facebook("com.restfb", "tracx.server.facebook", "tracx.server.apiHelpers"),
	HTTPClient("org.apache.http", "org.apache.commons.httpclient", "httpclient"),
	Jericho("net.htmlparser.jericho"),
	Twitter4J("twitter4j"),
	SOLR("org.apache.solr"),
	Gisgraphy("com.sirika", "com.gisgraphy"),
	MONGO("com.mongodb","org.mongodb"),
	Tor("org.silvertunnel.netlib"),
	JSch("com.jcraft.jsch"),
    Kafka("kafka"),
    Netty("io.netty"),
    Uima("org.apache.uima")
	;

	private String[] m_loggers = null;
	private TraceLevel m_level = TraceLevel.None;

	TraceModule()
	{
	}

	TraceModule(String... loggers)
	{
		m_loggers = loggers;
	}

	String[] getLoggers()
	{
		return m_loggers;
	}

	TraceLevel getTraceLevel()
	{
		return m_level;
	}
	void setTraceLevel(TraceLevel level)
	{
		m_level = level;
	}

	public static TraceModule valueOfIgnoreCase(String value)
	{
		for(TraceModule module : values())
			if(module.toString().equalsIgnoreCase(value))
				return module;
		return null;
	}
}
