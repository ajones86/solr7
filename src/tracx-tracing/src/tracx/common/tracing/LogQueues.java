/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2008 Tra.cx, All rights reserved                //
//                                                         //
//  Written by Yaniv Ben-Arie, June 2010                   //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	28/06/2010	Yaniv			Created
//
//
package tracx.common.tracing;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * Queuing log messages for the Log MBean.
 *
 * @author Yaniv
 *
 */
public class LogQueues
{
	/**
	 * Synch object for get instance
	 */
	private static Object ms_synch = new Object();

	/**
	 * The singleton instance
	 */
	private static LogQueues ms_instance = null;

	private final static int DEFAULT_SIZE 	= 1000;
	private final String 	 NEW_LINE 		= System.getProperty("line.separator");

	// Queues:
	private LinkedList<String> m_fatalQueue = null;
	private LinkedList<String> m_errorQueue = null;

	private int m_size = 1000;
	private long m_msgID = 0;



	/**
	 * Constructor
	 *
	 * @param size Maximum queues size
	 */
	private LogQueues(int size)
	{
		m_fatalQueue = new LinkedList<String>();
		m_errorQueue = new LinkedList<String>();

		m_size = size;
	}

	/**
	 * Gets the sole instance of LogQueues in the system.
	 *
	 * @param size Maximum queues size
	 * @return LogQueues
	 */
	public static LogQueues GetInstance(int size)
	{
		synchronized(ms_synch)
		{
			if (ms_instance == null)
			{
				ms_instance = new LogQueues(size);
			}
		}
		return ms_instance;
	}

	/**
	 * Gets the sole instance of LogQueues in the system.
	 *
	 * @return LogQueues
	 */
	public static LogQueues GetInstance()
	{
		return GetInstance(DEFAULT_SIZE);
	}

	/**
	 * return the maximum size of the queues
	 *
	 * @return the maximum size of the queues
	 */
	public int getMaximumSize()
	{
		return m_size;
	}

	/**
	 * Set the maximum size of the queues
	 *
	 * @param size - the new maximum size
	 */
	public void setMaximumSize(int size)
	{
		m_size = size;
	}

	/**
	 * Build the text message in the log4J format
	 *
	 * @param level Trace level
	 * @param msg The message it self
	 * @param t Exception - will take the stack trace (can be null)
	 * @return
	 */
	private String buildFinalMessage(TraceLevel level, String msg, Throwable t)
	{

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss.SSS");
        String now = formatter.format(date);

		if (t == null)
		{
			msg = String.format("%s [%s!] %s", now, level, msg);
		}
		else
		{
			msg = String.format("%s [%s!] %s %s %s", now, level, msg, NEW_LINE, getStackTrace(t));
		}
		return msg;
	}

	/**
	 * Build the text message in the log4J format
	 *
	 * @param level Trace level
	 * @param format The message it self with format patterns
	 * @param args arguments for the message
	 * @return
	 */
	private String buildMessage(TraceLevel level, String format, Object... args)
	{
		return buildFinalMessage(level, String.format(format, args), (Throwable)null);
	}

	/**
	 * Build the text message in the log4J format
	 *
	 * @param level Trace level
	 * @param format The message it self with format patterns
	 * @param t Exception - will take the stack trace (can be null)
	 * @param args arguments for the message
	 * @return
	 */
	private String buildMessage(TraceLevel level, String format, Throwable t, Object... args)
	{
		return buildFinalMessage(level, String.format(format, args), t);
	}

	/**
	 * Convert the aThrowable stack trace into string
	 * @param t
	 * @return String - the stackTrace in the standard form
	 */
	public static String getStackTrace(Throwable t)
	{
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    t.printStackTrace(printWriter);
	    return result.toString();
	 }

	/**
	 * Put a message inside a queue
	 *
	 * @param Q the queue to add the message to
	 * @param msg the message to put in the queue
	 */
	private void enqueue(LinkedList<String> Q, String msg)
	{
		synchronized(Q)
		{
			Q.add(String.format("[%d] %s", m_msgID, msg));
			m_msgID++;

			// Keep maximum size of the Q:
			if (Q.size() > m_size)
			{
				Q.removeFirst();
			}
		}
	}

	//////////////////////////////////
	//           Error Q            //
	//////////////////////////////////

	/**
	 * Insert error message to the Q
	 */
	public void error(TraceLevel level, String format, Throwable t, Object... args)
	{
		enqueue(m_errorQueue, buildMessage(level, format, t, args));
	}

	/**
	 * Insert error message to the Q
	 */
	public void error(TraceLevel level, String format, Throwable t)
	{
		enqueue(m_errorQueue, buildFinalMessage(level, format, t));
	}

	/**
	 * Insert error message to the Q
	 */
	public void error(TraceLevel level, String format, Object... args)
	{
		enqueue(m_errorQueue, buildMessage(level, format, args));
	}

	/**
	 * Insert error message to the Q
	 */
	public void error(TraceLevel level, String format)
	{
		enqueue(m_errorQueue, buildFinalMessage(level, format, null));
	}

	/**
	 * Copy the error queue to an array of Strings
	 * @return String[] - the error queue representation
	 */
	public String[] GetErrorQueue()
	{
		String [] result = new String[]{};
		synchronized(m_errorQueue)
		{
			result = m_errorQueue.toArray(result);
		}

		return result;
	}

	/**
	 * Clear the error queue
	 */
	public void clearErrorQueue()
	{
		synchronized(m_errorQueue)
		{
			m_errorQueue.clear();
		}
	}

	//////////////////////////////////
	//           Fatal Q            //
	//////////////////////////////////

	/**
	 * Insert fatal message to the Q
	 */
	public void fatal(TraceLevel level, String format, Throwable t, Object... args)
	{
		enqueue(m_fatalQueue, buildMessage(level, format, t, args));
	}

	/**
	 * Insert fatal message to the Q
	 */
	public void fatal(TraceLevel level, String format, Throwable t)
	{
		enqueue(m_fatalQueue, buildFinalMessage(level, format, t));
	}

	/**
	 * Insert fatal message to the Q
	 */
	public void fatal(TraceLevel level, String format, Object... args)
	{
		enqueue(m_fatalQueue, buildMessage(level, format, args));
	}

	/**
	 * Insert fatal message to the Q
	 */
	public void fatal(TraceLevel level, String format)
	{
		enqueue(m_fatalQueue, buildFinalMessage(level, format, null));
	}

	/**
	 * Copy the fatal queue to an array of Strings
	 * @return String[] - the fatal queue representation
	 */
	public String[] GetFatalQueue()
	{
		String [] result = new String[]{};
		synchronized(m_fatalQueue)
		{
			result = m_fatalQueue.toArray(result);
		}

		return result;
	}

	/**
	 * Clear the fatal queue
	 */
	public void clearFatalQueue()
	{
		synchronized(m_fatalQueue)
		{
			m_fatalQueue.clear();
		}
	}

}
