/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2012 Tra.cx, All rights reserved                //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	06/12/2007	Niv				Added non-static toL4JLevel()
//	01/12/2007	Niv				Created
//
//

package tracx.common.tracing;

import org.apache.logging.log4j.Level;

public enum TraceLevel
{
	All,
	Verbose,
	LowLevel,
	Debug,
	Info,
	Notice,
	Warn,
	Error,
	Fatal,
	//Default,
	None;

	public static TraceLevel valueOf(Level level)
	{
		if (level == Level.ALL) return All;
		else if (level == Level.TRACE) return Debug;
		else if (level == Level.DEBUG) return Debug;
		else if (level == Level.ERROR) return Error;
		else if (level == Level.FATAL) return Fatal;
		else if (level == Level.INFO) return Info;
		else if (level == Level.OFF) return None;
		else if (level == Level.WARN) return Warn;
		else if (level == CustomLevel.LOWLEVEL) return LowLevel;
		else if (level == CustomLevel.NOTICE) return TraceLevel.Notice;
		else if (level == CustomLevel.VERBOSE) return Verbose;
		else return Warn;
	}

	public static TraceLevel valueOfIgnoreCase(String value)
	{
		return valueOfIgnoreCase(value, null);
	}
	public static TraceLevel valueOfIgnoreCase(String value, TraceLevel defaultValue)
	{
		if(value == null) return defaultValue;

		if (value.equalsIgnoreCase(Level.ALL.toString())) return All;
		else if (value.equalsIgnoreCase(Level.DEBUG.toString()) || value.equalsIgnoreCase(Level.TRACE.toString())) return TraceLevel.Debug;
		else if (value.equalsIgnoreCase(Level.ERROR.toString())) return Error;
		else if (value.equalsIgnoreCase(Level.FATAL.toString())) return Fatal;
		else if (value.equalsIgnoreCase(Level.INFO.toString())) return Info;
		else if (value.equalsIgnoreCase(Level.OFF.toString())|| value.equalsIgnoreCase(None.name())) return None;
		else if (value.equalsIgnoreCase(Level.WARN.toString())) return Warn;
		else if (value.equalsIgnoreCase(CustomLevel.LOWLEVEL.toString()) || value.equalsIgnoreCase(LowLevel.name())) return LowLevel;		// CustomLevel.LOWLEVEL is now "LOW"
		else if (value.equalsIgnoreCase(CustomLevel.NOTICE.toString()) || value.equalsIgnoreCase(Notice.name())) return TraceLevel.Notice;	// CustomLevel.LOWLEVEL is now "NOTIC"
		else if (value.equalsIgnoreCase(CustomLevel.VERBOSE.toString()) || value.equalsIgnoreCase(Verbose.name())) return Verbose;			// CustomLevel.LOWLEVEL is now "VERBO"

		return defaultValue;
	}

	static Level toL4JLevel(TraceLevel level)
	{
		switch(level)
		{
			case All:		return Level.ALL;
			case Verbose:	return CustomLevel.VERBOSE;
			case LowLevel:	return CustomLevel.LOWLEVEL;
			case Debug:		return Level.DEBUG;
			case Info:		return Level.INFO;
			case Notice:	return CustomLevel.NOTICE;
			case Warn:		return Level.WARN;
			case Error:		return Level.ERROR;
			case Fatal:		return Level.FATAL;
			//case Default:	(goes together with none)
			case None:		return Level.OFF;
			default:		return Level.WARN;
		}
	}

	Level toL4JLevel()
	{
		return toL4JLevel(this);
	}

	static java.util.logging.Level toJULLevel(TraceLevel level)
	{
		switch(level)
		{
			case All:		return java.util.logging.Level.ALL;
			case Verbose:	return java.util.logging.Level.FINEST;
			case LowLevel:	return java.util.logging.Level.FINER;
			case Debug:		return java.util.logging.Level.FINE;
			case Info:		return java.util.logging.Level.INFO;
			case Notice:	return java.util.logging.Level.INFO;
			case Warn:		return java.util.logging.Level.WARNING;
			case Error:		return java.util.logging.Level.SEVERE;
			case Fatal:		return java.util.logging.Level.SEVERE;
			//case Default:	(goes together with none)
			case None:		return java.util.logging.Level.OFF;
			default:		return java.util.logging.Level.WARNING;
		}
	}

	java.util.logging.Level toJULLevel()
	{
		return toJULLevel(this);
	}
}
