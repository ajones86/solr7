/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2015 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Dec 2007                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	21/07/2014	Niv				Add support for AsyncAppender
//	12/01/2012	Niv				Added ExternalTracers
//  28/06/2010	Yaniv			Added log messages queuing for JMX support
//	29/01/2008	Niv				Added setModuleTraceLevel() / getModuleTraceLevel()
//	06/12/2007	Niv				Added dynamic setting of trace level for various modules (e.g. iBATIS, SQL)
//	04/12/2007	Niv				Disabled iBATIS and java.sql traces
//	03/12/2007	Niv				Added configuration file support
//	01/12/2007	Niv				Created
//
//

package tracx.common.tracing;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.IllegalFormatConversionException;
import java.util.List;
import java.util.Map;
import java.util.logging.Handler;

import org.apache.commons.configuration.Configuration;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.filter.ThresholdFilter;
import org.slf4j.bridge.SLF4JBridgeHandler;

public class Trace
{
	private static final String CONSOLE_PATTERN_PROPERTY = "console_pattern";
	private static final String FILE_PATTERN_PROPERTY = "file_pattern";
	private static final String ANSI_COLOR_PREFIX = "ansi_color_prefix";
	private static final String ANSI_COLOR_SUFFIX = "ansi_color_suffix";

	private static boolean m_initialized = false;
	private static boolean m_initializedByConfig = false;
	private static Logger m_rootLogger = LogManager.getRootLogger();

	private static List<Tracer> m_externalTracers = null;

	private static LogQueues ms_logQ = LogQueues.GetInstance(1000);
	private static boolean ms_jmxError = false;
	private static boolean ms_jmxFatal = false;
	private static List<java.util.logging.Logger> ms_loggers = new ArrayList<java.util.logging.Logger>(); //we keep the loggers since we don't want them garbaged collected 

	public static void initialize()
	{
		initialize(null);
	}
	
	public static void initialize(Configuration configuration)
	{
		initialize(configuration, null, null);
	}
	
	public static void initialize(Configuration configuration, String consolePattern, String filePattern)
	{
		if (m_initialized && m_initializedByConfig) return;
		m_initialized = true;
		m_initializedByConfig = configuration != null;
		
		// The XML configuration file can be updated via system properties
		boolean reconfigure = false;
		if (consolePattern != null && !consolePattern.isEmpty())
		{
			System.setProperty(CONSOLE_PATTERN_PROPERTY, consolePattern);
			reconfigure = true;
		}
		
		if (filePattern != null && !filePattern.isEmpty())
		{
			System.setProperty(FILE_PATTERN_PROPERTY, filePattern);
			reconfigure = true;
		}
		
		boolean enableANSI = false;
		if (configuration != null)
		{
			enableANSI = configuration.getBoolean("tracing.console.ansi", enableANSI);
			if (enableANSI)
			{
				System.setProperty(ANSI_COLOR_PREFIX, "%highlight{");
				System.setProperty(ANSI_COLOR_SUFFIX, "}");
				reconfigure = true;
			}
		}
		
		// Update the logger's configuration if there is a change in one of it's properties
		if (reconfigure)
		{
			((org.apache.logging.log4j.core.Logger) m_rootLogger).getContext().reconfigure();
		}
		
    	for(TraceModule module : TraceModule.values())
    	{
    		String propertyName = String.format("tracing.module.%s", module.toString().toLowerCase());
    		TraceLevel level = parseTraceLevel(configuration, propertyName);
    		setModuleTraceLevel(module, level);
    	}
    	
    	setMediaTraceLevel(TraceMedia.Console, parseTraceLevel(configuration, "tracing.console.level"));
    	setMediaTraceLevel(TraceMedia.File, parseTraceLevel(configuration, "tracing.file.level"));

    	// file
    	//m_logfileName = configuration.getString("tracing.file.name", m_logfileName);
    	//m_logfileExt = configuration.getString("tracing.file.ext", m_logfileExt);
    	//m_logfilePath = configuration.getString("tracing.file.path", m_logfilePath);
    	//m_logfileMaxSize = configuration.getInteger("tracing.file.maxsize", m_logfileMaxSize);
    	//m_logfileMaxFiles = configuration.getInteger("tracing.file.maxfiles", m_logfileMaxFiles);

		
		Configurator.setLevel(LogManager.ROOT_LOGGER_NAME, getLeastStrictTraceLevel().toL4JLevel());
		/*
    	LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		org.apache.logging.log4j.core.config.Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME); 
		loggerConfig.setLevel(getLeastStrictTraceLevel().toL4JLevel());
		ctx.updateLoggers();*/		
		// Print some startup debug info
		
		java.util.logging.Logger rootLogger = java.util.logging.Logger.getLogger("");
		Handler[] handlers = rootLogger.getHandlers();
		rootLogger.removeHandler(handlers[0]);
		SLF4JBridgeHandler.install();

		GlobalUncaughtExceptionHandler.setup();
	}

	private Trace()
	{
	}

	private static TraceLevel parseTraceLevel(Configuration properties, String propertyName)
	{
    	String property = properties.getString(propertyName);
    	if(property == null)
    		return null;

    	property = property.trim();
    	TraceLevel tl = TraceLevel.valueOfIgnoreCase(property);
    	if(tl == null)
    	{
    		System.err.println("Failed parsing property " + propertyName + ": \"" + property + "\"");
    		return null;
    	}

    	return tl;
	}

	
	/**
	 * Gets the trace level for the root logger
	 * @return
	 */
	private static TraceLevel getTraceLevel()
	{
		return getTraceLevel((String) null);
	}
	
	private static TraceLevel getTraceLevel(TraceMedia media)
	{
		Filter f = null;
		if (media == TraceMedia.Console)
		{
			AbstractAppender appender = getConsoleAppender();
			f = appender.getFilter();
		}
		else if (media == TraceMedia.File)
		{
			AbstractAppender appender = getFileAppender();
			f = appender.getFilter();
		}
		
		return TraceLevel.valueOfIgnoreCase(f.toString()); // Rely on that the filter's toString() method returns the level name!
	}
	
	private static AbstractAppender getConsoleAppender()
	{
		return ((org.apache.logging.log4j.core.Logger)m_rootLogger).getContext().getConfiguration().getAppender(TraceMedia.Console.getAppenderName());
	}
	
	private static AbstractAppender getFileAppender()
	{
		return ((org.apache.logging.log4j.core.Logger)m_rootLogger).getContext().getConfiguration().getAppender(TraceMedia.File.getAppenderName());
	}
	
	private static boolean setMediaTraceLevel(TraceMedia media, TraceLevel value)
	{
		if(value == null)
			return false;
		Filter f;
		if (media == TraceMedia.Console)
		{
			AbstractAppender appender = getConsoleAppender();
			f = appender.getFilter();
			appender.removeFilter(f);
			appender.addFilter(ThresholdFilter.createFilter(value.toL4JLevel(), f.getOnMatch(), f.getOnMismatch()));
			return true;
		}
		else if (media == TraceMedia.File)
		{
			AbstractAppender appender = getFileAppender();
			f = appender.getFilter();
			appender.removeFilter(f);
			appender.addFilter(ThresholdFilter.createFilter(value.toL4JLevel(), f.getOnMatch(), f.getOnMismatch()));
			return true;
		}
		
		return false;
	}
	
	/**
	 * Gets the trace level according to the logger's name
	 */
	private static TraceLevel getTraceLevel(String loggerName)
	{
		Logger logger = getLoggerByName(loggerName);
		
		if(logger == null)
			return TraceLevel.Fatal; //this might happen probably when we shutdown

		// First check if the logger is in the configuration
		LoggerContext ctx = ((org.apache.logging.log4j.core.Logger) logger).getContext();
		org.apache.logging.log4j.core.config.Configuration config = ctx.getConfiguration();
		if (loggerName != null && config.getLoggers().containsKey(loggerName))
		{
			LoggerConfig loggerConfig = config.getLoggerConfig(loggerName);
			return TraceLevel.valueOf(loggerConfig.getLevel());
		}
		
		// If it's not configured, get the logger's tracing level
		return TraceLevel.valueOf(logger.getLevel());
	}

	private static TraceLevel getAssureEnabledTraceLevel(TraceLevel desired)
	{
		TraceLevel level = getMostStrictTraceLevel();
		if (level.toL4JLevel().compareTo(desired.toL4JLevel()) >= 0)
		{
			return desired;
		}
		
		return level;
	}
	
	private static TraceLevel getMostStrictTraceLevel()
	{
		TraceLevel consoleLevel = getTraceLevel(TraceMedia.Console);
		TraceLevel fileLevel = getTraceLevel(TraceMedia.File);
		
		if (consoleLevel == TraceLevel.None)
		{
			return fileLevel;
		}
		else if (fileLevel == TraceLevel.None)
		{
			return consoleLevel;
		}
		
		return consoleLevel.toL4JLevel().compareTo(fileLevel.toL4JLevel()) >= 0 ? fileLevel : consoleLevel;
	}
	
	private static TraceLevel getLeastStrictTraceLevel(TraceMedia media, TraceLevel mediaLevel)
	{
		TraceLevel otherLevel = media == TraceMedia.Console ? getTraceLevel(TraceMedia.File) : getTraceLevel(TraceMedia.Console);
		return mediaLevel.toL4JLevel().compareTo(otherLevel.toL4JLevel()) >= 0 ? mediaLevel : otherLevel;
	}

	private static TraceLevel getLeastStrictTraceLevel()
	{
		TraceLevel consoleLevel = getTraceLevel(TraceMedia.Console);
		TraceLevel fileLevel = getTraceLevel(TraceMedia.File);
		return consoleLevel.toL4JLevel().compareTo(fileLevel.toL4JLevel()) >= 0 ? consoleLevel : fileLevel;
	}		

	/**
	 * Sets the trace level for the given media type
	 * @param media
	 * @param value
	 * @throws Exception
	 */
	public static void setTraceLevel(TraceMedia media, TraceLevel value)
	{
		setTraceLevel(media, (String) null, value);
	}
	
	/**
	 * Sets the trace level according to the logger's name
	 * @param loggerName - the wanted logger name
	 * @param value - the new trace level
	 * @throws Exception
	 */
	private static void setTraceLevel(String loggerName, TraceLevel value)
	{
		setTraceLevel(null, loggerName, value);
	}

	private static void setTraceLevel(TraceMedia media, String loggerName, TraceLevel value)
	{
		if(!m_initialized) initialize();
		
		Logger logger = getLoggerByName(loggerName);
		if (!(logger instanceof org.apache.logging.log4j.core.Logger))
		{
			traceAssure(TraceLevel.Notice, "Trace.setTraceLevel: unable to set the logger's trace level (logger is instance of %s)", logger.getClass().getName());
			return;
		}
		
		String msg = null;
		if (media != null)
		{
			// Check the new level is different
			TraceLevel level = getTraceLevel(media);
			if (!level.equals(value))
			{
				// Update the level filter (in the configuration file) for the given media type
				if (setMediaTraceLevel(media, value)) msg = String.format("%s logging level changed to %s", media.name(), value);
				
				// Update the root logger's level to the highest between all the media types
				TraceLevel newRootLevel = getLeastStrictTraceLevel(media, value);
				if (!newRootLevel.equals(getTraceLevel()))
				{
					Configurator.setLevel("", value.toL4JLevel());
					msg = String.format("%s. General logger level changed to %s", msg, newRootLevel);
				}
				
				((org.apache.logging.log4j.core.Logger) m_rootLogger).getContext().updateLoggers();
			}
			else
			{
				msg = String.format("%s logging level is already set to %s", media.name(), value);
			}
			
			traceAssure(TraceLevel.Notice, msg);
		}
		// When changing the root level, change the appenders' filter level as well
		else
		{	
			Level level = logger.getLevel();
			String name = logger.getName() == null || logger.getName().equals(LogManager.ROOT_LOGGER_NAME) ? "Root" : logger.getName();
			if (!level.equals(value.toL4JLevel()))
			{
				
				// If it's the root logger, change the appenders levels as well
				if (logger.equals(m_rootLogger))
				{
					setMediaTraceLevel(TraceMedia.Console, value);
					setMediaTraceLevel(TraceMedia.File, value);
				}

				// Update the configuration, if the logger exists there
				updateConfigLevel(loggerName, value);
		
				msg = String.format("Logger \"%s\" trace level changed to %s", name, value);
			}
			else
			{
				msg = String.format("Logger %s trace level is already %s", name, value);
			}

			traceAssure(TraceLevel.Notice, msg, value);
		}
	}

	private static void updateConfigLevel(String loggerName, TraceLevel value)
	{
		Configurator.setLevel(loggerName, value.toL4JLevel());
		Logger logger = getLoggerByName(loggerName);
		LoggerContext ctx = ((org.apache.logging.log4j.core.Logger) logger).getContext();
		LoggerConfig loggerConfig = ((org.apache.logging.log4j.core.Logger) logger).getContext().getConfiguration().getLoggerConfig(loggerName);
		loggerConfig.setAdditive(false);
		ctx.updateLoggers();
	}
	
	private static Logger getLoggerByName(String loggerName)
	{
		if (loggerName == null || loggerName.equals(LogManager.ROOT_LOGGER_NAME))
		{
			return m_rootLogger;
		}
		else
		{
			return LogManager.getLogger(loggerName);
		}
	}

	private static TraceLevel getModuleTraceLevel(TraceModule module)
	{
		if(module == null) throw new IllegalArgumentException("module is null");
		return module.getTraceLevel();
	}


	public static void setModuleTraceLevel(TraceModule module, TraceLevel value)
	{
		if(module == null) throw new IllegalArgumentException("module is null");
		if(value == null) 
			return;
		
		module.setTraceLevel(value);
		for(String loggerName : module.getLoggers())
		{
			setTraceLevel(loggerName, value);
			org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(loggerName);
			logger.setLevel(OldLevel.toL4JValue(value));
			
			java.util.logging.Logger l = java.util.logging.Logger.getLogger(loggerName);
			l.setLevel(OldLevel.toJULValue(value));
			
			ms_loggers.add(l);
		}
	}
//	public static Map<String, TraceLevel> getModuleTraceLevel(TraceModule module)
//	{
//		if(module == null) throw new IllegalArgumentException("module is null");
//		
//		Map<String, TraceLevel> levels = new HashMap<String, TraceLevel>();
//		for (String loggerName : module.getLoggers())
//		{
//			TraceLevel level = getTraceLevel(loggerName);
//			levels.put(loggerName, level);
//		}
//		
//		return levels;
//	}
//
//	public static void setModuleTraceLevel(TraceModule module, TraceLevel value)
//	{
//		if(module == null) throw new IllegalArgumentException("module is null");
//
//		Map<String, TraceLevel> existingLevels = getModuleTraceLevel(module);
//		for(String loggerName : module.getLoggers())
//		{
//			if (existingLevels != null && existingLevels.containsKey(loggerName) && existingLevels.get(loggerName).toL4JLevel().compareTo(value.toL4JLevel()) == 0)
//			{
//				traceAssure(TraceLevel.Notice, "%s trace level is already set to %s", loggerName, value);
//				continue;
//			}
//			
//			try
//			{
//				setTraceLevel(loggerName, value);
//				
//				org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(loggerName);
//				logger.setLevel(OldLevel.toL4JValue(value));
//				
//				java.util.logging.Logger javaLogger = java.util.logging.Logger.getLogger(loggerName);
//				javaLogger.setLevel(OldLevel.toJULValue(value));
//				
//				ms_loggers.add(javaLogger);
//				
//				traceAssure(TraceLevel.Notice, "'%s' trace level changed to %s", loggerName, value);
//			}
//			catch (Exception e)
//			{
//				if (Trace.warn()) Trace.warn("failed to set %s trace level", loggerName);
//			}
//		}
//	}

	public static void addExternalTracer(Tracer tracer)
	{
		if(m_externalTracers == null)
		{
			synchronized(Trace.class)
			{
				if(m_externalTracers == null)
				{
					m_externalTracers = new ArrayList<Tracer>(1);
					m_externalTracers.add(tracer);
					return;
				}
			}
		}
		synchronized(m_externalTracers)
		{
			m_externalTracers.add(tracer);
		}
	}

    private final static ThreadLocal<DateFormat> ms_dateFormat_ddMMyyyy_HHmmss = new ThreadLocal<DateFormat>() {
        @Override
		protected synchronized DateFormat initialValue() { return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); }
    };

    private static String formatDateTime(Date date)
	{
		if(date == null)
			return "null";
		else
			return ms_dateFormat_ddMMyyyy_HHmmss.get().format(date);
	}

	private static void log(TraceLevel level, String msg, Throwable t)
	{
		if(m_externalTracers != null)
		{
			String formatted = String.format("%s %-5s [%s]: %s", formatDateTime(new Date()), level.toString(), Thread.currentThread().getName(), msg);

			if(m_externalTracers != null)
			{
				synchronized(m_externalTracers)
				{
					for(Tracer tracer : m_externalTracers)
					{
						try
						{
							synchronized(tracer)
							{
								if(checkTraceLevel(tracer.getTraceLevel(), level))
								{
									tracer.println(formatted);
									if(t != null)
									{
										t.printStackTrace(tracer);
									}
								}
							}
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}

		m_rootLogger.log(level.toL4JLevel(), msg, t);
	}

	private static String arrayToString(Object[] a, String separator)
	{
	    StringBuilder result = new StringBuilder();
	    if(a.length > 0)
	    {
	        result.append(a[0].toString());
	        for(int i = 1; i < a.length; i++)
	        {
	            result.append(separator);
	            result.append(a[i].toString());
	        }
	    }
	    return result.toString();
	}

	private static String stringFormat(String format, Object... args)
	{
		try
		{
			return String.format(format, args);
		}
		catch(IllegalFormatConversionException e)
		{
			String msg = String.format("IllegalFormatConversionException '%s': format='%s', args='%s'", e.getMessage(), format, arrayToString(args, ","));
			//traceAssure(TraceLevel.Error, msg, e);
			return msg;
		}
	}

	public static void trace(TraceLevel level, String msg, Throwable t)
	{
		if(!m_initialized) initialize();

		if((level == TraceLevel.None) || !checkTraceLevel(level))
		{
			msg = String.format("[%s!] %s", level, msg);
			level = TraceLevel.Fatal;
		}

		log(level, msg, t);
	}
	public static void trace(TraceLevel level, String format, Object... args)
	{
		trace(level, stringFormat(format, args), (Throwable)null);
	}
	public static void trace(TraceLevel level, String format, Throwable t, Object... args)
	{
		trace(level, stringFormat(format, args), t);
	}
	
	public static void trace(TraceMedia media, TraceLevel level, String msg, Throwable t)
	{
		if(!m_initialized) initialize();

		if((level == TraceLevel.None) || !checkTraceLevel(media, level))
		{
			msg = String.format("[%s!] %s", level, msg);
			level = TraceLevel.Fatal;
		}

		log(level, msg, t);
	}
	public static void trace(TraceMedia media, TraceLevel level, String format, Object... args)
	{
		trace(media, level, stringFormat(format, args), (Throwable)null);
	}
	public static void trace(TraceMedia media, TraceLevel level, String format, Throwable t, Object... args)
	{
		trace(media, level, stringFormat(format, args), t);
	}

	public static void traceAssure(TraceLevel level, String msg)
	{
		trace(getAssureEnabledTraceLevel(level), msg, (Throwable)null);
	}
	public static void traceAssure(TraceLevel level, String msg, Throwable t)
	{
		trace(getAssureEnabledTraceLevel(level), msg, t);
	}
	public static void traceAssure(TraceLevel level, String format, Object... args)
	{
		trace(getAssureEnabledTraceLevel(level), stringFormat(format, args), (Throwable)null);
	}
	public static void traceAssure(TraceLevel level, String format, Throwable t, Object... args)
	{
		trace(getAssureEnabledTraceLevel(level), stringFormat(format, args), t);
	}
	
	public static void startJMXError()										{ms_jmxError=true;}
	public static void stopJMXError()										{ms_jmxError=false;}
	public static void startJMXFatal()										{ms_jmxFatal=true;}
	public static void stopJMXFatal()										{ms_jmxFatal=false;}

	public static void verbose(String msg)									{ trace(TraceLevel.Verbose, msg, (Throwable)null); }
	public static void verbose(String msg, Throwable t)						{ trace(TraceLevel.Verbose, msg, t); }
	public static void verbose(String format, Object... args)				{ trace(TraceLevel.Verbose, format, args); }
	public static void verbose(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Verbose, format, t, args); }

	public static void lowlevel(String msg)									{ trace(TraceLevel.LowLevel, msg, (Throwable)null); }
	public static void lowlevel(String msg, Throwable t)					{ trace(TraceLevel.LowLevel, msg, t); }
	public static void lowlevel(String format, Object... args)				{ trace(TraceLevel.LowLevel, format, args); }
	public static void lowlevel(String format, Throwable t, Object... args) { trace(TraceLevel.LowLevel, format, t, args); }

	public static void debug(String msg)									{ trace(TraceLevel.Debug, msg, (Throwable)null); }
	public static void debug(String msg, Throwable t)						{ trace(TraceLevel.Debug, msg, t); }
	public static void debug(String format, Object... args)					{ trace(TraceLevel.Debug, format, args); }
	public static void debug(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Debug, format, t, args); }

	public static void info(String msg)										{ trace(TraceLevel.Info, msg, (Throwable)null); }
	public static void info(String msg, Throwable t)						{ trace(TraceLevel.Info, msg, t); }
	public static void info(String format, Object... args)					{ trace(TraceLevel.Info, format, args); }
	public static void info(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Info, format, t, args); }

	public static void notice(String msg)									{ trace(TraceLevel.Notice, msg, (Throwable)null); }
	public static void notice(String msg, Throwable t)						{ trace(TraceLevel.Notice, msg, t); }
	public static void notice(String format, Object... args)				{ trace(TraceLevel.Notice, format, args); }
	public static void notice(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Notice, format, t, args); }

	public static void warn(String msg)										{ trace(TraceLevel.Warn, msg, (Throwable)null); }
	public static void warn(String msg, Throwable t)						{ trace(TraceLevel.Warn, msg, t); }
	public static void warn(String format, Object... args)					{ trace(TraceLevel.Warn, format, args); }
	public static void warn(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Warn, format, t, args); }

	public static void error(String msg)									{ trace(TraceLevel.Error, msg, (Throwable)null); if(ms_jmxError)ms_logQ.error(TraceLevel.Error, msg); }
	public static void error(String msg, Throwable t)						{ trace(TraceLevel.Error, msg, t); if(ms_jmxError)ms_logQ.error(TraceLevel.Error, msg, t); }
	public static void error(String format, Object... args)					{ trace(TraceLevel.Error, format, args); if(ms_jmxError)ms_logQ.error(TraceLevel.Error, format, args); }
	public static void error(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Error, format, t, args); if(ms_jmxError)ms_logQ.error(TraceLevel.Error, format, t, args); }

	public static void fatal(String msg)									{ trace(TraceLevel.Fatal, msg, (Throwable)null); if(ms_jmxFatal)ms_logQ.fatal(TraceLevel.Fatal, msg); }
	public static void fatal(String msg, Throwable t)						{ trace(TraceLevel.Fatal, msg, t); if(ms_jmxFatal)ms_logQ.fatal(TraceLevel.Fatal, msg, t); }
	public static void fatal(String format, Object... args)					{ trace(TraceLevel.Fatal, format, args); if(ms_jmxFatal)ms_logQ.fatal(TraceLevel.Fatal, format, args); }
	public static void fatal(String format, Throwable t, Object... args) 	{ trace(TraceLevel.Fatal, format, t, args); if(ms_jmxFatal)ms_logQ.fatal(TraceLevel.Fatal, format, t, args); }

	public static boolean verbose()		{ return checkTraceLevel(TraceLevel.Verbose);	}
	public static boolean lowlevel()	{ return checkTraceLevel(TraceLevel.LowLevel);	}
	public static boolean debug()		{ return checkTraceLevel(TraceLevel.Debug);		}
	public static boolean info()		{ return checkTraceLevel(TraceLevel.Info);		}
	public static boolean notice()		{ return checkTraceLevel(TraceLevel.Notice);	}
	public static boolean warn()		{ return checkTraceLevel(TraceLevel.Warn);		}
	public static boolean error()		{ return checkTraceLevel(TraceLevel.Error);		}
	public static boolean fatal()		{ return checkTraceLevel(TraceLevel.Fatal);		}

	
	public static boolean checkTraceLevel(TraceLevel level)
	{
		return checkTraceLevel(getTraceLevel(), level);
	}
	
	public static boolean checkTraceLevel(TraceModule module, TraceLevel level)
	{
		 return checkTraceLevel(Trace.getModuleTraceLevel(module), level);		
	}
		
	private static boolean checkTraceLevel(TraceMedia media, TraceLevel level)
	{
		return checkTraceLevel(getTraceLevel(media), level);
	}
	
	private static boolean checkTraceLevel(TraceLevel current, TraceLevel level)
	{
		return TraceLevel.All.equals(current) || (current.toL4JLevel().compareTo(level.toL4JLevel()) >= 0);
	}
	
	/**
	 * Convert from log4j2 level to log4j1 level (for the modules loggers - which are still using log4j1)
	 */
	@SuppressWarnings("serial")
	private static class OldLevel extends org.apache.log4j.Level
	{
		private static final int VERBOSE_INT  	= DEBUG_INT - 2000;
		private static final int LOWLEVEL_INT 	= DEBUG_INT - 1000;
		private static final int NOTICELEVEL_INT = INFO_INT  + 1000;
		
		private static final int SYSLOG_VERBOSE_INT  = org.apache.log4j.Level.DEBUG.getSyslogEquivalent();
		private static final int SYSLOG_LOWLEVEL_INT = org.apache.log4j.Level.DEBUG.getSyslogEquivalent();
		private static final int SYSLOG_NOTICE_INT = org.apache.log4j.Level.INFO.getSyslogEquivalent();

		public static final OldLevel VERBOSE  = new OldLevel(VERBOSE_INT,     "VERBO", SYSLOG_VERBOSE_INT);
		public static final OldLevel LOWLEVEL = new OldLevel(LOWLEVEL_INT,    "LOW",   SYSLOG_LOWLEVEL_INT);
		public static final OldLevel NOTICE   = new OldLevel(NOTICELEVEL_INT, "NOTIC", SYSLOG_NOTICE_INT);
		
		protected OldLevel(int level, String levelStr, int syslogEquivalent)
		{
			super(level, levelStr, syslogEquivalent);
		}
		
		protected static org.apache.log4j.Level toL4JValue(TraceLevel log4j2Level)
		{
			if (log4j2Level == TraceLevel.All) return org.apache.log4j.Level.ALL;
			else if (log4j2Level == TraceLevel.Debug) return org.apache.log4j.Level.DEBUG;
			else if (log4j2Level == TraceLevel.Error) return org.apache.log4j.Level.ERROR;
			else if (log4j2Level == TraceLevel.Fatal) return org.apache.log4j.Level.FATAL;
			else if (log4j2Level == TraceLevel.Info) return org.apache.log4j.Level.INFO;
			else if (log4j2Level == TraceLevel.None) return org.apache.log4j.Level.OFF;
			else if (log4j2Level == TraceLevel.Warn) return org.apache.log4j.Level.WARN;
			else if (log4j2Level == TraceLevel.LowLevel) return LOWLEVEL;
			else if (log4j2Level == TraceLevel.Verbose) return VERBOSE;
			else if (log4j2Level == TraceLevel.Notice) return NOTICE;
			else return org.apache.log4j.Level.WARN;
		}
		
		protected static java.util.logging.Level toJULValue(TraceLevel log4j2Level)
		{
			if (log4j2Level == TraceLevel.All) return java.util.logging.Level.ALL;
			else if (log4j2Level == TraceLevel.Debug) return java.util.logging.Level.FINE;
			else if (log4j2Level == TraceLevel.Error) return java.util.logging.Level.SEVERE;
			else if (log4j2Level == TraceLevel.Fatal) return java.util.logging.Level.SEVERE;
			else if (log4j2Level == TraceLevel.Info) return java.util.logging.Level.INFO;
			else if (log4j2Level == TraceLevel.None) return java.util.logging.Level.OFF;
			else if (log4j2Level == TraceLevel.Warn) return java.util.logging.Level.WARNING;
			else if (log4j2Level == TraceLevel.LowLevel) return java.util.logging.Level.FINER;
			else if (log4j2Level == TraceLevel.Verbose) return java.util.logging.Level.FINEST;
			else if (log4j2Level == TraceLevel.Notice) return java.util.logging.Level.INFO;
			else return java.util.logging.Level.WARNING;
		}
	}
}
