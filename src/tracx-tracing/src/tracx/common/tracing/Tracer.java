/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2012 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2012                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	12/01/2012	Niv				Created
//
//

package tracx.common.tracing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 * Will never get messages with TraceLevel less than Console/File
 */
public class Tracer extends PrintWriter
{
	private TraceLevel m_level = TraceLevel.All;
	private File m_file = null;
	private OutputStream m_out = null;
	private String m_fileName = null;
	private Writer m_writer = null;

	public Tracer(File file) throws FileNotFoundException
	{
		super(file);
		m_file = file;
	}

	public Tracer(File file, String csn) throws FileNotFoundException, UnsupportedEncodingException
	{
		super(file, csn);
		m_file = file;
	}

	public Tracer(OutputStream out)
	{
		super(out);
		m_out = out;
	}

	public Tracer(OutputStream out, boolean autoFlush)
	{
		super(out, autoFlush);
		m_out = out;
	}

	public Tracer(String fileName) throws FileNotFoundException
	{
		super(fileName);
		m_fileName = fileName;
	}

	public Tracer(String fileName, String csn) throws FileNotFoundException, UnsupportedEncodingException
	{
		super(fileName, csn);
		m_fileName = fileName;
	}

	public Tracer(Writer out)
	{
		super(out);
		m_writer = out;
	}

	public Tracer(Writer out, boolean autoFlush)
	{
		super(out, autoFlush);
		m_writer = out;
	}

	/*package*/ File getFile() { return m_file; }
	/*package*/ OutputStream getOutputStram() { return m_out; }
	/*package*/ String getFileName() { return m_fileName; }
	/*package*/ Writer getWriter() { return m_writer; }


	public Tracer setTraceLevel(TraceLevel level)
	{
		m_level = level;
		return this;
	}
	public TraceLevel getTraceLevel()
	{
		return m_level;
	}
}
