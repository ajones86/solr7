/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2012 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Jan 2012                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	12/01/2012	Niv				Created
//
//

package tracx.common.tracing;

import java.io.StringWriter;

public class StringTracer extends Tracer
{
	public StringTracer()
	{
		super(new StringWriter());
	}

	@Override
	public String toString()
	{
		return getWriter().toString();
	}

	public synchronized String clear()
	{
		String contents = toString();
		((StringWriter)getWriter()).getBuffer().setLength(0);
		return contents;
	}
}
