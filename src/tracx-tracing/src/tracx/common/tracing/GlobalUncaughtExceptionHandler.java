/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Debug Tracing Module                             //
//  © 2006-2015 Tracx, All rights reserved                 //
//                                                         //
//  Written by Niv Singer, Mar 2015                        //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	26/03/2015	Niv				Created
//
//

package tracx.common.tracing;

import java.lang.Thread.UncaughtExceptionHandler;

public class GlobalUncaughtExceptionHandler implements UncaughtExceptionHandler
{
	private static GlobalUncaughtExceptionHandler INSTANCE = new GlobalUncaughtExceptionHandler();

	public static void setup()
	{
		Thread.setDefaultUncaughtExceptionHandler(INSTANCE);
	}


	private UncaughtExceptionHandler m_prevDefault = null;

	private GlobalUncaughtExceptionHandler()
	{
		m_prevDefault = Thread.getDefaultUncaughtExceptionHandler();
	}

	@Override
	public void uncaughtException(Thread t, Throwable e)
	{
		try
		{
			String msg = String.format("Uncaught Exception in %s, thread is dead.", t);
			System.err.println(msg);
			e.printStackTrace();
			if(Trace.fatal()) Trace.fatal(msg, e);
		}
		catch(Throwable oops)
		{
			try
			{
				System.err.println("Exception caught in GlobalUncaughtExceptionHandler");
				oops.printStackTrace();
			}
			catch(Throwable well_what_can_you_do)
			{
			}
		}

		try
		{
			if(m_prevDefault != null)
			{
				m_prevDefault.uncaughtException(t, e);
			}
		}
		catch(Throwable oops)
		{
			try
			{
				System.err.println("Exception caught in GlobalUncaughtExceptionHandler when invoking previous default handler");
				oops.printStackTrace();
			}
			catch(Throwable well_what_can_you_do)
			{
			}
		}
	}
}
