package org.apache.lucene.analysis.en;

import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

/**
 * This class wrapps the Kstem algorithm, so we'll be able to use it inside our code
 */
public class KstemmerWrapper 
{
	private KStemmer stemmer = null;
	private Method stem = null;
	public KstemmerWrapper()
	{
		try
		{
			Constructor<KStemmer> kStemmerConstructor = KStemmer.class.getDeclaredConstructor();
			kStemmerConstructor.setAccessible(true);
			stemmer = kStemmerConstructor.newInstance();
			stem = stemmer.getClass().getDeclaredMethod("stem", String.class);
			stem.setAccessible(true);
		}
		catch(Exception e)
		{
			System.out.println("KstemmerWrapper instantiation failed with "+e.getMessage());
		}
	}

	public String stem(String string)
	{
		try
		{
			return (String)stem.invoke(stemmer,string.toLowerCase());
		}
		catch(Exception e)
		{
			System.out.println("KstemmerWrapper stem failed with "+e.getMessage());
			return null;
		}
	}

	/*public String stemOld(String string)
	{
		try
		{
			return stemmerOld.stem(string.toLowerCase());
		}
		catch(Exception e)
		{
			System.out.println("KstemmerWrapper stem failed with "+e.getMessage());
			return null;
		}
	}

	private KStemAnalyzer m_analyzer = new KStemAnalyzer();
	public String stemNew(String string)
	{
		StringBuilder sb = new StringBuilder();
        try 
        {
            Reader reader = new StringReader(string);
            TokenStream stream = m_analyzer.tokenStream("", reader);
			while (stream.incrementToken()) 
			{
			    CharTermAttribute term = stream.getAttribute(CharTermAttribute.class);
			    if(term != null)
			    {
			    	sb.append(term).append(" ");
			    }
			}
		} 
        catch (Exception e) 
        {
			System.out.println("KstemmerWrapper stemNew failed with "+e.getMessage());
		}
        return sb.toString().trim();
	}
	
	static class KStemAnalyzer extends Analyzer 
	{
        public final TokenStream tokenStream(String fieldName, Reader reader) 
        {
            TokenStream result = new LowerCaseTokenizer(Version.LUCENE_36, reader);
            result = new KStemFilter(result);
            return result;
        }
    }
	
	public static void main(String[] args)
	{
		KstemmerWrapper wrapper = new KstemmerWrapper();
		
		for(int i=0; i<100000; i++)
		{
			wrapper.stem(RandomStringUtils.random(5));
		}
		for(int i=0; i<100000; i++)
		{
			wrapper.stemOld(RandomStringUtils.random(5));
		}

		long before = System.currentTimeMillis();
		for(int i=0; i<10000000; i++)
		{
			wrapper.stem(RandomStringUtils.random(5));
		}
		System.out.println(System.currentTimeMillis() - before);

		before = System.currentTimeMillis();
		for(int i=0; i<10000000; i++)
		{
			wrapper.stemOld(RandomStringUtils.random(5));
		}
		System.out.println(System.currentTimeMillis() - before);
	}
	public static void main(String[] args)
	{
		KstemmerWrapper wrapper = new KstemmerWrapper();
			System.out.println(wrapper.stem("Runners"));
	}*/

}
