package tracx.server.index.solr.search;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.solr.common.params.FacetParams;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.handler.component.FacetComponent;
import org.apache.solr.handler.component.PivotFacetHelper;
import org.apache.solr.handler.component.PivotFacetProcessor;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.request.SimpleFacets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TracxFacet extends FacetComponent
{
	public static Logger log = LoggerFactory.getLogger(TracxFacet.class);

	  /**
	   * Actually run the query
	   */
	  @Override
	  public void process(ResponseBuilder rb) throws IOException {

	    //SolrParams params = rb.req.getParams();
	    if (rb.doFacets) {
	      ModifiableSolrParams params = new ModifiableSolrParams();
	      SolrParams origParams = rb.req.getParams();
	      Iterator<String> iter = origParams.getParameterNamesIterator();
	      while (iter.hasNext()) {
	        String paramName = iter.next();
	        // Deduplicate the list with LinkedHashSet, but _only_ for facet params.
	        if (paramName.startsWith(FacetParams.FACET) == false) {
	          params.add(paramName, origParams.getParams(paramName));
	          continue;
	        }
	        HashSet<String> deDupe = new LinkedHashSet<>(Arrays.asList(origParams.getParams(paramName)));
	        params.add(paramName, deDupe.toArray(new String[deDupe.size()]));
	      }

	      SimpleFacets f = new TracxAdvancedFacets(rb.req, rb.getResults().docSet, params, rb);
	      
	      NamedList<Object> counts = FacetComponent.getFacetCounts(f);
	      String[] pivots = params.getParams(FacetParams.FACET_PIVOT);
	      if (pivots != null && pivots.length > 0) {
	        PivotFacetProcessor pivotProcessor 
	          = new PivotFacetProcessor(rb.req, rb.getResults().docSet, params, rb);
	        SimpleOrderedMap<List<NamedList<Object>>> v 
	          = pivotProcessor.process(pivots);
	        if (v != null) {
	          counts.add(PIVOT_KEY, v);
	        }
	      }
	      
	      rb.rsp.add("facet_counts", counts);
	    }
	  }

	  static final String PIVOT_KEY = "facet_pivot";
	  	  
}
