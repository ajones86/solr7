package tracx.server.index.solr.search;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.request.SimpleFacets;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.schema.SchemaField;
import org.apache.solr.search.DocIterator;
import org.apache.solr.search.DocSet;
import org.apache.solr.search.SolrIndexSearcher;
import org.apache.solr.util.BoundedTreeSet;
import org.apache.solr.common.params.FacetParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TracxAdvancedFacets extends SimpleFacets 
{
	public static Logger log = LoggerFactory.getLogger(TracxAdvancedFacets.class);
	public static final int MAX_HITS = 10000;
	public static final String FACET_MAX_HITS = FacetParams.FACET + ".maxHits";

	public static final int MAX_SECONDS = 30;
	public static final String FACET_MAX_SECONDS = FacetParams.FACET + ".maxSeconds";

	/** Value for FACET_METHOD param to indicate that Solr should use our advanced method
	   * 
	   */
	  public static final String FACET_METHOD_advanced = "advanced";

	// per-facet values
	/*SolrParams localParams; // localParams on this particular facet command
	  String facetValue;      // the field to or query to facet on (minus local params)
	  DocSet base;            // the base docset for this particular facet
	  String key;             // what name should the results be stored under
	 */
	public TracxAdvancedFacets(SolrQueryRequest req, DocSet docs, SolrParams params, ResponseBuilder rb) 
	{
		super(req, docs, params, rb);
	}

	@Override
	public NamedList<Integer> getTermCounts(String field, ParsedParams parsed) throws IOException
	{
		SolrParams params = parsed.params;

		SchemaField sf = searcher.getSchema().getField(field);
		boolean advancedMethod = params.getFieldParam(field, FacetParams.FACET_METHOD).equals("advanced") && sf.storeTermVector();
		if(!advancedMethod)
		{
			return super.getTermCounts(field, parsed);
		}

	    int offset = params.getFieldInt(field, FacetParams.FACET_OFFSET, 0);
	    int limit = params.getFieldInt(field, FacetParams.FACET_LIMIT, 100);
	    if (limit == 0) return new NamedList<Integer>();
	    Integer mincount = params.getFieldInt(field, FacetParams.FACET_MINCOUNT);
	    if (mincount==null) {
	      Boolean zeros = params.getFieldBool(field, FacetParams.FACET_ZEROS);
	      // mincount = (zeros!=null && zeros) ? 0 : 1;
	      mincount = (zeros!=null && !zeros) ? 1 : 0;
	      // current default is to include zeros.
	    }
	    boolean missing = params.getFieldBool(field, FacetParams.FACET_MISSING, false);
	    // default to sorting if there is a limit.
	    String sort = params.getFieldParam(field, FacetParams.FACET_SORT, limit>0 ? FacetParams.FACET_SORT_COUNT : FacetParams.FACET_SORT_INDEX);
	    String prefix = params.getFieldParam(field,FacetParams.FACET_PREFIX);
		int maxDocs = rb.req.getParams().getInt(FACET_MAX_HITS, MAX_HITS);
		int maxSeconds = rb.req.getParams().getInt(FACET_MAX_SECONDS, MAX_SECONDS);
	    NamedList<Integer> counts = getFacetTermAdvancedCounts(searcher, parsed.docs, field, offset, limit, mincount,missing,sort,prefix, maxDocs, maxSeconds);

		return counts;
	}

	public NamedList<Integer> getFacetTermAdvancedCounts(SolrIndexSearcher searcher, DocSet docs, String field, int offset, int limit, int mincount, boolean missing, String sort, String prefix, int maxDocs, int maxSeconds) throws IOException 
	{
		HashMap<String, Integer> countMap = new HashMap<String, Integer>();
		NamedList<Integer> countList = new NamedList<Integer>();
		long maxTime = System.currentTimeMillis() + 1000 * maxSeconds;
		

		// Because the results will return 0 or any other number of results requested we cannot
		// count on that and we need to annotate the number of documents from the hits in the log.
		DocSet docSet = docs;//TODO???rb.getResults().docSet;
		DocIterator docSetIterator = docSet.iterator();

		Integer hits = docSet.size();
		if(hits.equals(0))
		{
			return countList;
		}
		log.info("docSet.size: " + hits);

		int rate = Math.max(hits,maxDocs) / maxDocs;
		log.info("rate: " + rate);
		int counter = 0;
		IndexReader reader = searcher.getIndexReader();

		String[] terms;
		int[] freq;

		// Going over all the documents collecting terms and their frequencies using the
		// term vector.
		while(docSetIterator.hasNext())
		{
			if(System.currentTimeMillis() >= maxTime)
			{
				log.info("reached maxTime");
				break;
			}				
			Integer i = docSetIterator.next();
			if(counter++ % rate != 0)
				continue;
			//log.info("docID: {}", i);
			Terms result = reader.getTermVector( i, field);
			if((result == null) || (result.size() == 0))
			{
				//log.info("Empty result for document " + i);
				continue;
			}

			//terms = result.getTerms();
			//freq = result.getTermFrequencies();
			// Validation check.
			//if(freq.length != terms.length) return countList;
			TermsEnum termsEnum = result.iterator();
			BytesRef text;
		    while((text = termsEnum.next()) != null) 
		    {
		    	String term	= text.utf8ToString();
				if (prefix!=null && !term.startsWith(prefix)) continue;
				
				Integer count = countMap.get(term);
				if(count == null)
				{
					// First encounter
					count = 0;
				}

				count += 1;//freq[j];
				countMap.put(term, count);
			}
		}

		// Sorting the results and respecting the limit factor
		final BoundedTreeSet<CountPair<String,Integer>> queue = new BoundedTreeSet<CountPair<String,Integer>>(limit);
		for(Entry<String, Integer> entry : countMap.entrySet())
		{
			queue.add(new CountPair<String, Integer>(entry.getKey(), entry.getValue()));
		}

		countMap.clear();	// Clearing the map as soon as possible to save space.

		for(CountPair<String, Integer> p : queue)
		{
			if(p.val < mincount) break;
			countList.add(p.key, p.val);
		}
		return countList;
	}	
}
