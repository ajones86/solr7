package tracx.server.index.solr.search;

import java.io.IOException;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.solr.handler.component.QueryComponent;
import org.apache.solr.handler.component.ResponseBuilder;

public class RegexSolrComponent extends QueryComponent
{
//	private static final String REGEX = "\\b\\d{5,10}\\b"; // \b\d{3,5}\b
	private static final String REGEX_ATTR = "regex:";
	private static final String REGEX_FIELD = "regex";
	private static final String REAL_FIELD = "contents";

	@Override
	public void process(ResponseBuilder rb) throws IOException
	{
//		// This is the fastest way to check if the query needs to be regex modified.
//		// The reason is that the string is already set in the response builder - it's a member.
//		if(rb.getQueryString().contains(REGEX_ATTR))
//		{
//			Query q = modifyQuery(rb.getQuery());
//			rb.setQuery(q);
//		}
//
//		// NOTE: This call for the super process is only needed if component replaces the query component in the
//		// component array. If it stands for itself in the array - this invocation is forbidden!
		 super.process(rb);
	}

//	// Modifying the query using recurse.
//	// Note that we going down to the terms of the query if the boolean query contains something that
//	// needs to be modified - other wise we leave it alone - this will save a lot of time.
//	private Query modifyQuery(Query q)
//	{
//		// Recurse inside the boolean query.
//		if(q instanceof BooleanQuery)
//		{
//			for(BooleanClause cl :((BooleanQuery)q).clauses())
//			{
//				Query qc = cl.getQuery();
//
//				// No need to recurse inside clauses without regex query inside them.
//				if(!qc.toString().contains(REGEX_ATTR))
//				{
//					continue;
//				}
//
//				cl.setQuery(modifyQuery(qc));
//			}
//		}
//
//		// We can only modify term query to RegexQuery for this is how Lucene works - SO DONT BLAME ME!
//		if(q instanceof TermQuery)
//		{
//			Term term = ((TermQuery)q).getTerm();
//
//			String field = term.field();
//			if(!field.equals(REGEX_FIELD))	// I'm not sure I need this - but let's be on the safe size.
//			{
//				return q;
//			}
//
//			term = modifyRegexToken(term);
//			q = new RegexQuery(term);
//
//			// We will use the Java regular regex capabilities with a pattern that is equal to
//			// the term's text itself.
//			JavaUtilRegexCapabilities impl = new JavaUtilRegexCapabilities();
//			impl.compile(term.text());
//
//			((RegexQuery)q).setRegexImplementation(impl);
//		}
//
//		return q;
//	}
//
//	/**
//	 * Creates the new term to the regex query according to the given input.
//	 *
//	 * @param term
//	 * @return modified term
//	 */
//	private Term modifyRegexToken(Term term)
//	{
//		return new Term(REAL_FIELD, term.text());
//	}
}

