package tracx.server.index.solr.search;

import java.io.IOException;

import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.params.TermsParams;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.TermsComponent;

public class MultipleTermsComponent extends TermsComponent
{
	private static final String TERMS_MULTI = "terms.multi";

	@Override
	public void process(ResponseBuilder rb) throws IOException
	{
		SolrParams params = rb.req.getParams();

		String termsString = params.get(TERMS_MULTI, null);
		if((termsString == null)  || (termsString.length()==0))
		{
			super.process(rb);
		}
		else
		{
			String[] termsToFind = termsString.split(",");
			ModifiableSolrParams newParams = new ModifiableSolrParams(params);
			rb.req.setParams(newParams);
			newParams.set(TermsParams.TERMS_UPPER_INCLUSIVE,String.valueOf(true));
			newParams.set(TermsParams.TERMS_LOWER_INCLUSIVE,String.valueOf(true));
			for(String termStr : termsToFind)
			{
				newParams.set(TermsParams.TERMS_LOWER,termStr);
				newParams.set(TermsParams.TERMS_UPPER,termStr);
				super.process(rb);
			}
		}
	}	
	
/*
	public void process1(ResponseBuilder rb) throws IOException
	{
	    SolrParams params = rb.req.getParams();
	    if (!params.getBool(TermsParams.TERMS, false)) return;

	    String[] fields = params.getParams(TermsParams.TERMS_FIELD);

	    NamedList<Object> termsResult = new SimpleOrderedMap<Object>();
	    rb.rsp.add("terms", termsResult);

	    if (fields == null || fields.length==0) return;

	    int limit = params.getInt(TermsParams.TERMS_LIMIT, 10);
	    if (limit < 0) {
	      limit = Integer.MAX_VALUE;
	    }

	    String lowerStr = params.get(TermsParams.TERMS_LOWER);
	    String upperStr = params.get(TermsParams.TERMS_UPPER);
	    boolean upperIncl = params.getBool(TermsParams.TERMS_UPPER_INCLUSIVE, false);
	    boolean lowerIncl = params.getBool(TermsParams.TERMS_LOWER_INCLUSIVE, true);
	    boolean sort = !TermsParams.TERMS_SORT_INDEX.equals(
	        params.get(TermsParams.TERMS_SORT, TermsParams.TERMS_SORT_COUNT));
	    int freqmin = params.getInt(TermsParams.TERMS_MINCOUNT, 1);
	    int freqmax = params.getInt(TermsParams.TERMS_MAXCOUNT, UNLIMITED_MAX_COUNT);
	    if (freqmax<0) {
	      freqmax = Integer.MAX_VALUE;
	    }
	    String prefix = params.get(TermsParams.TERMS_PREFIX_STR);
	    String regexp = params.get(TermsParams.TERMS_REGEXP_STR);

	    boolean raw = params.getBool(TermsParams.TERMS_RAW, false);


	    final AtomicReader indexReader = rb.req.getSearcher().getAtomicReader();
	    Fields lfields = indexReader.fields();

	    for (String field : fields) {
	      NamedList<Integer> fieldTerms = new NamedList<Integer>();
	      termsResult.add(field, fieldTerms);

	      Terms terms = lfields == null ? null : lfields.terms(field);
	      if (terms == null) {
	        // no terms for this field
	        continue;
	      }

	      FieldType ft = raw ? null : rb.req.getSchema().getFieldTypeNoEx(field);
	      if (ft==null) ft = new StrField();

	      // prefix must currently be text
	      BytesRef prefixBytes = prefix==null ? null : new BytesRef(prefix);

	      BytesRef upperBytes = null;
	      if (upperStr != null) {
	        upperBytes = new BytesRef();
	        ft.readableToIndexed(upperStr, upperBytes);
	      }

	      BytesRef lowerBytes;
	      if (lowerStr == null) {
	        // If no lower bound was specified, use the prefix
	        lowerBytes = prefixBytes;
	      } else {
	        lowerBytes = new BytesRef();
	        if (raw) {
	          // TODO: how to handle binary? perhaps we don't for "raw"... or if the field exists
	          // perhaps we detect if the FieldType is non-character and expect hex if so?
	          lowerBytes = new BytesRef(lowerStr);
	        } else {
	          lowerBytes = new BytesRef();
	          ft.readableToIndexed(lowerStr, lowerBytes);
	        }
	      }


	     TermsEnum termsEnum = terms.iterator(null);
	     BytesRef term = null;

	      if (lowerBytes != null) {
	        if (termsEnum.seekCeil(lowerBytes) == TermsEnum.SeekStatus.END) {
	          termsEnum = null;
	        } else {
	          term = termsEnum.term();
	          //Only advance the enum if we are excluding the lower bound and the lower Term actually matches
	          if (lowerIncl == false && term.equals(lowerBytes)) {
	            term = termsEnum.next();
	          }
	        }
	      } else {
	        // position termsEnum on first term
	        term = termsEnum.next();
	      }

	      int i = 0;
	      BoundedTreeSet<CountPair<BytesRef, Integer>> queue = (sort ? new BoundedTreeSet<CountPair<BytesRef, Integer>>(limit) : null);
	      CharsRef external = new CharsRef();
	      while (term != null && (i<limit || sort)) {
	        boolean externalized = false; // did we fill in "external" yet for this term?

	        // stop if the prefix doesn't match
	        if (prefixBytes != null && !StringHelper.startsWith(term, prefixBytes)) break;

	        if (upperBytes != null) {
	          int upperCmp = term.compareTo(upperBytes);
	          // if we are past the upper term, or equal to it (when don't include upper) then stop.
	          if (upperCmp>0 || (upperCmp==0 && !upperIncl)) break;
	        }

	        // This is a good term in the range.  Check if mincount/maxcount conditions are satisfied.
	        int docFreq = termsEnum.docFreq();
	        if (docFreq >= freqmin && docFreq <= freqmax) {
	          // add the term to the list
	          if (sort) {
	            queue.add(new CountPair<BytesRef, Integer>(BytesRef.deepCopyOf(term), docFreq));
	          } else {

	            // TODO: handle raw somehow
	            if (!externalized) {
	              ft.indexedToReadable(term, external);
	            }
	            fieldTerms.add(external.toString(), docFreq);
	            i++;
	          }
	        }

	        term = termsEnum.next();
	      }

	      if (sort) {
	        for (CountPair<BytesRef, Integer> item : queue) {
	          if (i >= limit) break;
	          ft.indexedToReadable(item.key, external);          
	          fieldTerms.add(external.toString(), item.val);
	          i++;
	        }
	      }
	    }
	}
*/
}
