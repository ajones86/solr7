/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2010 Tracx, All rights reserved                 //
//                                                         //
//  Written by Moshe Lichman, Oct 2010                     //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	29/10/2010	Moshe			Created
//
//

package tracx.server.index.analysis.exactPhraseAnalyser;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenFilterFactory;

public abstract class ExactPhraseFilterFactoryBase extends TokenFilterFactory implements ResourceLoaderAware
{
	public ExactPhraseFilterFactoryBase(Map<String, String> args) 
	{
	    super(args);
	    m_isMirskiSolr = getBoolean(args, MIRSKI, false);
		m_firstGroupSymbols = get(args, FIRST_GROUP_SYMBOL);

	    
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}



	private volatile static boolean ms_isfirstGroupSymbolLoaded = false;
	private static final String MIRSKI = "mirski";
	private static final String FIRST_GROUP_SYMBOL = "firstGroupOfSymbols";

	protected static HashSet<Character> ms_firstGroupOfSymbols = new HashSet<Character>();
	protected boolean m_isMirskiSolr = false;
	protected String m_firstGroupSymbols = null;


	public void inform(ResourceLoader loader)
	{
		if(m_firstGroupSymbols != null)
		{
			try
			{
				if(!ms_isfirstGroupSymbolLoaded)
				{
					synchronized(ms_firstGroupOfSymbols)
					{
						if(ms_isfirstGroupSymbolLoaded)
						{

							return;
						}

						List<String> firstGroupSymbolList = getLines(loader, m_firstGroupSymbols.trim());
						for(String symbol : firstGroupSymbolList)
						{
							ms_firstGroupOfSymbols.add(symbol.toCharArray()[0]);
						}

						// Adding '#' for it cannot be read from the file for the loader think it's a comment.
						ms_firstGroupOfSymbols.add('#');

						ms_isfirstGroupSymbolLoaded = true;
					}
				}
			}

			catch(IOException e)
			{
				throw new RuntimeException(e);
			}
		}
		else
		{
			throw new IllegalArgumentException("ExactPhraseFilterFactoryBase: Missing file to load symbols from!");
		}
	}
}
