/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2010 Tracx, All rights reserved                 //
//                                                         //
//  Written by Moshe Lichman, Oct 2010                     //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	29/10/2010	Moshe			Created
//
//

package tracx.server.index.analysis.exactPhraseAnalyser;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

public abstract class ExactPhraseFilterBase extends TokenFilter
{
	public final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	public final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	public final PositionIncrementAttribute posIncAtt = addAttribute(PositionIncrementAttribute.class);

	protected final static String             EXACT_SUFFIX     = "$EXACT";
	protected final static HashSet<Character> ms_legalPrefixes = new HashSet<Character>();

	static
	{
		ms_legalPrefixes.add('+');
		ms_legalPrefixes.add('#');
		ms_legalPrefixes.add('@');
		ms_legalPrefixes.add('$');
		ms_legalPrefixes.add('(');
	}

	//NEW
	protected LinkedHashSet<String> m_suffixes = new LinkedHashSet<String>();
	protected LinkedList<TermData> m_termsCollection = new LinkedList<TermData>();

	protected String m_termAsString;
	protected String m_prefix = null;
	protected boolean m_hasStoredToken = false;
	protected boolean m_hasTokensCollection = false;
	protected int m_startOffset = 0;

	protected MyStringTokenizer m_termTokens = null;
	protected Set<Character> m_symbolsSet;

	public ExactPhraseFilterBase(TokenStream input, Set<Character> symbolSet)
	{
		super(input);
		m_symbolsSet = symbolSet;
	}
	
	@Override
	public void reset() throws IOException 
	{
		super.reset();
		m_termTokens = null;
		m_termsCollection.clear();
		m_hasTokensCollection = false;
		m_hasStoredToken = false;
		m_suffixes.clear();
		m_termAsString = null;
		m_prefix = null;		
		m_startOffset = 0;
	}
	
	
	protected abstract LinkedList<TermData> getTermsCollector(String term);


	private String getPrefix(String rawTerm)
	{
		String prefixReturn = "";
		int i = 0;
		int prefixesLength;

		while (m_symbolsSet.contains(rawTerm.charAt(i)))
		{
			i++;
			if (i >= rawTerm.length()) break;
		}

		prefixesLength = i - 1;

		if ((prefixesLength < 0) || (prefixesLength >= rawTerm.length()))
		{
			return prefixReturn;
		}
		char prefix = rawTerm.charAt(prefixesLength);
		if (ms_legalPrefixes.contains(prefix))
		{
//			prefixesSet.add(String.valueOf(prefix));
			prefixReturn = String.valueOf(prefix);
		}


		return prefixReturn;
	}

	private LinkedHashSet<String> getSuffixes(String rawTerm)
	{
		LinkedHashSet<String> suffixesSet = new LinkedHashSet<String>();
		int i = rawTerm.length() - 1;
		int cleanTermLength;

		while(m_symbolsSet.contains(rawTerm.charAt(i)))
		{
			i--;
			if(i < 0) break;
		}

		cleanTermLength = i + 1;
		if((cleanTermLength < 1) || (cleanTermLength >= rawTerm.length()))
		{
			return suffixesSet;
		}


		String suffixes = rawTerm.substring(cleanTermLength);

		// Adding all the suffixes as one.
		suffixesSet.add(suffixes);

		if(suffixes.length() >= 2)
		{
			String doubleSuffix = suffixes.substring(0, 2);
			if(doubleSuffix.equals("?!") || doubleSuffix.equals("!?"))
			{
				suffixesSet.add(doubleSuffix);
			}
		}

		if(suffixes.length() >= 1)
		{
			// Adding just a single suffix
			suffixesSet.add(suffixes.substring(0, 1));
		}

		return suffixesSet;
	}

	@Override
	public final boolean incrementToken() throws IOException
	{
			while(true)
			{
				if(!m_hasStoredToken)
				{
					if(!input.incrementToken())
					{
						return false;
					}
				}
	
				if(!m_hasTokensCollection)
				{
					m_termAsString = new String(termAtt.buffer()).substring(0, termAtt.length());
					m_startOffset = offsetAtt.startOffset();
					clearAttributes();
					m_prefix = getPrefix(m_termAsString);
					m_suffixes = getSuffixes(m_termAsString);
					m_termTokens = new MyStringTokenizer(m_termAsString, m_symbolsSet.toString());
					
	

					if(!m_termTokens.hasMoreTokens())
					{
						continue;
					}
	
					m_hasStoredToken = true;
					m_hasTokensCollection = true;
					m_termsCollection = getTermsCollector(m_termAsString);
				}
	
				if((m_termsCollection == null) || m_termsCollection.isEmpty())
				{
					m_hasStoredToken = false;
					m_hasTokensCollection = false;

					continue;
				}
	
				TermData termData = m_termsCollection.iterator().next();
				m_termsCollection.remove(termData);
	
				setAttribute(termData);
	
				return true;
			}
	}

	protected static class TermData
	{
		private String m_term;
		private boolean m_positionInc;
		private int m_offset;

		public TermData(String term, boolean positionInc, int offset)
		{
			m_term = term;
			m_positionInc = positionInc;
			m_offset = offset;
		}

		public String getTerm()
		{
			return m_term;
		}

		public boolean getPositionInc()
		{
			return m_positionInc;
		}
		
		public int getOffset()
		{
			return m_offset;
		}
	}


	/**
	 * Create the attribute to be stored in the SOLR from the string term that is given.
	 *
	 * @param term - The word we wish to index
	 * @param allowPositionIncrement - true if the word should be indexed in separate cell then the previous one.
	 *                                 This is used when indexing "Awake?" for example when we want to index both
	 *                                 "Awake" and "Awake" in the same position.
	 * @param flag - Indicated whether or not this word will be processed by the stamming analyzer
	 *               Note that this do nothing but letting the stamming analyzer that it need to process the word
	 *               so in case the stamming analyzer is not registered then nothing will happen.
	 */
	public void setAttribute(TermData termData)
	{
		// Keeping the current start offset before clearing the attribute.
		//int startOffset = offsetAtt.startOffset();

		clearAttributes();
		termAtt.setEmpty().append(termData.getTerm());
		offsetAtt.setOffset(m_startOffset + termData.getOffset(),  m_startOffset + termData.getOffset() + termData.getTerm().length());
		posIncAtt.setPositionIncrement(termData.getPositionInc()?1:0);
	}

	protected boolean isEmptyOrNull(String text)
	{
		if((text == null) || text.trim().equals(""))
		{
			return true;
		}

		return false;
	}
}