/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2010 Tracx, All rights reserved                 //
//                                                         //
//  Written by Moshe Lichman, Oct 2010                     //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	29/10/2010	Moshe			Created
//
//

package tracx.server.index.analysis.exactPhraseAnalyser;

import java.util.Map;

import org.apache.lucene.analysis.TokenStream;

public class ExactPhraseIndexFilterFactory extends ExactPhraseFilterFactoryBase
{
	public ExactPhraseIndexFilterFactory(Map<String, String> args) {
	    super(args);
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

	public TokenStream create(TokenStream input)
	{
		return new ExactPhraseIndexFilter(input, ms_firstGroupOfSymbols, m_isMirskiSolr);
	}
}
