/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2010 Tracx, All rights reserved                 //
//                                                         //
//  Written by Moshe Lichman, Oct 2010                     //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	29/10/2010	Moshe			Created
//
//

package tracx.server.index.analysis.exactPhraseAnalyser;

import java.util.HashSet;
import java.util.LinkedList;
import org.apache.lucene.analysis.TokenStream;

public class ExactPhraseIndexFilter extends ExactPhraseFilterBase
{
	private static final String TWITTER_MENTION_ATTR = "@";
	private static final String TWITTER_HASHTAG_ATTR = "#";
	private static final String GPLUS_MENTION_ATTR = "+";

	private boolean m_isMirskiSolr = true;

	public ExactPhraseIndexFilter(TokenStream input, HashSet<Character> firstGroupOfSymbols, boolean indexMentions)
	{
		super(input, firstGroupOfSymbols);
		m_isMirskiSolr = indexMentions;
	}

	@Override
	protected LinkedList<TermData> getTermsCollector(String term)
	{
		LinkedList<TermData> termList = new LinkedList<TermData>();
		if(isEmptyOrNull(term))
		{
			return termList;
		}

		String currSubTerm = "";

		boolean isFirstSubWord = true;
		boolean isSpecialWord = false;

		// If it's Mirski's SOLR then we index special terms (Mentions, HashTags, GooglePlus mentions)
		// in a special way:
		//  1. Hashtags are being indexed as hashtags and that's it (without suffixes).
		//  2. Mentions are not being indexed.
		if(m_isMirskiSolr)
		{
			if(!isEmptyOrNull(m_prefix))
			{
				if(m_prefix.equals(TWITTER_MENTION_ATTR) || m_prefix.equals(GPLUS_MENTION_ATTR))
				{
					return null;
				}
				if(m_prefix.equals(TWITTER_HASHTAG_ATTR))
				{
					termList.add(new TermData(term, true, 0));
					return termList;
				}
			}
		}

		if(m_isMirskiSolr)
		{
			if(!isEmptyOrNull(m_prefix))
			{
				currSubTerm = m_termTokens.nextToken();

				if(m_prefix.equals("#"))
				{
					termList.add(new TermData(String.format("%s%s", m_prefix, currSubTerm), true, 0));
				}

				isFirstSubWord = false;
				isSpecialWord = true;
			}
		}

		while(m_termTokens.hasMoreTokens())
		{
			currSubTerm = m_termTokens.nextToken();
			termList.add(new TermData(currSubTerm, true, m_termTokens.currentPosition() - currSubTerm.length()));

			isSpecialWord = false;

			if(isFirstSubWord)
			{
				isFirstSubWord = false;
				if(!isEmptyOrNull(m_prefix))
				{
					//termList.add(new TermData(String.format("%s%s", m_prefix, currSubTerm), false, m_termTokens.currentPosition()-1));
					termList.add(new TermData(String.format("%s%s", m_prefix, currSubTerm), false, m_termTokens.currentPosition() - currSubTerm.length() - m_prefix.length()));
				}
			}
		}

		if(isSpecialWord)
		{
			return termList;
		}

		for(String suffix : m_suffixes)
		{
			termList.add(new TermData(String.format("%s%s", currSubTerm, suffix), false, m_termTokens.currentPosition() - currSubTerm.length()));
		}

		return termList;
	}
}
