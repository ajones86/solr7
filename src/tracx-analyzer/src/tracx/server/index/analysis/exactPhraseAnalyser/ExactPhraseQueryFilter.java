/////////////////////////////////////////////////////////////
//                                                         //
//  Tracx Server                                           //
//  © 2006-2010 Tracx, All rights reserved                 //
//                                                         //
//  Written by Moshe Lichman, Oct 2010                     //
//                                                         //
/////////////////////////////////////////////////////////////
//
//	Date		Programmer		Description
//	29/10/2010	Moshe			Created
//
//

package tracx.server.index.analysis.exactPhraseAnalyser;

import java.util.HashSet;
import java.util.LinkedList;
import org.apache.lucene.analysis.TokenStream;

public class ExactPhraseQueryFilter extends ExactPhraseFilterBase
{

	public ExactPhraseQueryFilter(TokenStream input, HashSet<Character> symbolMap)
	{
		super(input, symbolMap);
	}

	// In the query we decided to filter the term as a whole term and put white spaces
	// instead of symbols in the middle of the word.
	// For example: The term "Awake?!!" will be filtered as "awake?!!" (not as in the index.).
	// and the word "Aw@ke?!!" will be filtered as "aw" and "ke?!!" with 0 words between them.

//	@Override
//	public boolean indexNextSubWord()
//	{
//		try
//		{
//			StringBuilder nextSubWord = new StringBuilder(m_termTokens.nextToken());
//			if(!m_termTokens.hasMoreTokens())
//			{
//				m_hasStoredToken = false;
//				nextSubWord.append(m_suffixSymbols);
//			}
//
//			setAttribute(nextSubWord.toString(), true, 0);
//			return true;
//		}
//		catch(Exception e)
//		{
//			if(Trace.debug()) Trace.debug("ExactPhraseQueryFilter: Failed to index term [%s].\n", m_termAsString);
//
//			// Exception occurred so we want to skip the current word.
//			m_hasStoredToken = false;
//			return false;
//		}
//	}

	@Override
//	protected LinkedHashMap getTermsCollector()
	protected LinkedList<TermData> getTermsCollector(String term)
	{
//		LinkedHashMap tokensCollection = new LinkedHashMap ();
		LinkedList<TermData> tokensCollection = new LinkedList<TermData>();

		boolean isFirstSubWord = true;
		while(m_termTokens.hasMoreTokens())
		{
			String token = m_termTokens.nextToken();
			if(isFirstSubWord && !isEmptyOrNull(m_prefix))
			{
				token = String.format("%s%s", m_prefix, token);
				isFirstSubWord = false;
			}
			else if(!m_termTokens.hasMoreTokens() && !m_suffixes.isEmpty())
			{
				token = String.format("%s%s", token, m_suffixes.iterator().next());
			}

			tokensCollection.add(new TermData(token, true, m_termTokens.currentPosition()));
//			tokensCollection.put(token, true);
		}

		return tokensCollection;
	}
}
