package tracx.server.index.analysis.hebrewAnalyzer;

import java.util.Map;

import hebmorph.StreamLemmatizer;

public class HebrewAnalyzerQueryFactory extends HebrewAnalyzerFactory
{
	public HebrewAnalyzerQueryFactory(Map<String, String> args) {
	    super(args);
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

	/**
	 * Overriding so it will not lemmetize words from the query.
	 */
	@Override
	protected StreamLemmatizer getStreamLemmatizer()
	{
		return new StreamLemmatizer(true);
	}
}
