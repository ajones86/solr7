package tracx.server.index.analysis.hebrewAnalyzer;

import hebmorph.StreamLemmatizer;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.hebrew.StreamLemmasFilter;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenFilterFactory;
import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;
import org.apache.lucene.util.Version;

public class HebrewAnalyzerFactory extends TokenizerFactory implements ResourceLoaderAware
{
	public HebrewAnalyzerFactory(Map<String, String> args) 
	{
	    super(args);
		m_stopWordsFile = get(args, "stop_words");
	    
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

//	private volatile static boolean ms_isStopWordsLoaded = false;
	private static final String HSPELL_PATH ="./prop/hspell-data-files";
	protected CharArraySet ms_stopWords = new CharArraySet(10, true);

	private static Object ms_syncObject = new Object();
	private String m_stopWordsFile = null;
	
	public void inform(ResourceLoader loader)
	{
		try
		{
//			if(ms_isStopWordsLoaded)
//			{
//				return;
//			}

			synchronized(ms_syncObject)
			{
//				if(ms_isStopWordsLoaded) return;

				if(m_stopWordsFile != null)
				{
					List<String> words = getLines(loader, m_stopWordsFile.trim());
					for(String word : words)
					{
						ms_stopWords.add(word);
					}
				}

//				ms_isStopWordsLoaded = true;
			}
		}
		catch(IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	protected StreamLemmatizer getStreamLemmatizer()
	{
		return new StreamLemmatizer();
	}

	@Override
	public Tokenizer create(AttributeFactory arg0)
	{
		StreamLemmatizer hebMorphLemmatizer = getStreamLemmatizer();
		try
		{
			hebMorphLemmatizer.initFromHSpellFolder(HSPELL_PATH, true, false);
		}
		catch(IOException e)
		{
			throw new RuntimeException(e);
		}

		return new StreamLemmasFilter(hebMorphLemmatizer, ms_stopWords, null, true);
	}
}
