package tracx.server.index.analysis.hebrewAnalyzer;

import hebmorph.StreamLemmatizer;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.Analyzer.TokenStreamComponents;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.hebrew.StreamLemmasFilter;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.util.Version;

public class HebrewAnalyzer extends Analyzer
{

	private static StreamLemmatizer hebMorphLemmatizer = new StreamLemmatizer();
	private static final String hspellPath ="./prop/hspell-data-files";
	private static final CharArraySet stopWords = new CharArraySet(10, true);

	static
	{
		hebMorphLemmatizer = new StreamLemmatizer();
		try
		{
			stopWords.add("לפי");
			hebMorphLemmatizer.initFromHSpellFolder(hspellPath, true, false);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public HebrewAnalyzer()
	{}

	@Override
	protected TokenStreamComponents createComponents(String fieldName)
	{
		Tokenizer tokenizer = new StreamLemmasFilter(hebMorphLemmatizer, stopWords, null, false);
		
		TokenStream result = new LowerCaseFilter(tokenizer);

		return new TokenStreamComponents(tokenizer, result); 
	}

}
