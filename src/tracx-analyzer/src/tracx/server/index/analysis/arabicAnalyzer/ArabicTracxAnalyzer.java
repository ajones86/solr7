//package tracx.server.index.analysis.arabicAnalyzer;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.InputStream;
//import java.io.Reader;
//import java.util.HashSet;
//import java.util.Scanner;
//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.TokenStream;
//import org.apache.lucene.analysis.ar.ArabicAnalyzer;
//import org.apache.lucene.util.Version;
//import tracx.common.tracing.Trace;
//
//public class ArabicTracxAnalyzer extends Analyzer
//{
//	private static final String COMMENT_CHAR = "#";
//	private static final String STOP_WORDS_PATH = "./solr/conf/arabic_stopwords.txt";
//	private static HashSet<String> ms_stopWords = new HashSet<String>();
//	static
//	{
//		try
//		{
//			InputStream stopWords = loadResourceStream(STOP_WORDS_PATH);
//			Scanner scanner = new Scanner(stopWords, "UTF-8");
//
//			while(scanner.hasNext())
//			{
//				String term = scanner.nextLine();
//				if(term.startsWith(COMMENT_CHAR))
//				{
//					continue;
//				}
//
//				ms_stopWords.add(term.toLowerCase());
//			}
//		}
//		catch(Exception e)
//		{
//			if(Trace.warn()) Trace.warn("ArabicTracxAnalyzerTwo: Failed to load stop words list from path '%s'", STOP_WORDS_PATH);
//		}
//	}
//
//	@Override
//	public TokenStream tokenStream(String fieldName, Reader reader)
//	{
//		ArabicAnalyzer analyzer = new ArabicAnalyzer(Version.LUCENE_33, ms_stopWords);
//		TokenStream result = analyzer.tokenStream(fieldName, reader);
//
//		return result;
//	}
//
//	/**
//	 * Loads resources regardless if it's running from the server or from the servlet.
//	 *
//	 * @param path - The path to the specific file.
//	 *
//	 * @return Input stream of the file or null if file not found.
//	 */
//	private static InputStream loadResourceStream(String path)
//	{
//		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//		String classpath = path.replace(File.separatorChar, '/');
//
//		InputStream stream = classLoader.getResourceAsStream(classpath);
//
//		// For non servlet purposes
//		if (stream == null)
//		{
//			try
//			{
//				stream = new FileInputStream(classpath);
//			}
//			catch (FileNotFoundException e)
//			{
//				if(Trace.debug()) Trace.debug("Helpers.loadResourceStream: Could not load stream %s", path);
//				return null;
//			}
//		}
//
//		return stream;
//	}
//}
