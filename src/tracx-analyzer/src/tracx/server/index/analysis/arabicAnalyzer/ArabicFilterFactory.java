package tracx.server.index.analysis.arabicAnalyzer;

import java.util.Map;

import gpl.pierrick.brihaye.aramorph.lucene.ArabicStemmer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.TokenFilterFactory;

public class ArabicFilterFactory extends TokenFilterFactory
{
	public ArabicFilterFactory(Map<String, String> args) {
	    super(args);
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

	@Override
	public TokenStream create(TokenStream input)
	{
		return new ArabicStemmer(input, false, false);
	}
}
