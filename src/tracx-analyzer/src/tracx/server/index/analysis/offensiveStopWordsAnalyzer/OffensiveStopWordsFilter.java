package tracx.server.index.analysis.offensiveStopWordsAnalyzer;

import java.io.IOException;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.FilteringTokenFilter;

public class OffensiveStopWordsFilter extends FilteringTokenFilter
{
	private CharArraySet m_offensive;
	private boolean m_isExact = false;
	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

	public OffensiveStopWordsFilter(TokenStream input, CharArraySet offensiveStopWords, boolean isExact)
	{
		super(input);
		m_offensive = offensiveStopWords;
		m_isExact = isExact;
	}

	@Override
	protected boolean accept() throws IOException
	{
		if(m_offensive == null)
		{
			return true;
		}

		String termAsString = new String(termAtt.buffer(), 0, termAtt.length());

		if(m_isExact)
		{
			StringBuilder term = new StringBuilder();
			for(char c : termAsString.toCharArray())
			{
				if(Character.isLetterOrDigit(c))
				{
					term.append(c);
				}
			}

			termAsString = term.toString();
		}

		return !m_offensive.contains(termAsString);
	}
}
