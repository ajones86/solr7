package tracx.server.index.analysis.offensiveStopWordsAnalyzer;

import java.util.Map;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenFilterFactory;

public class OffensiveStopWordsFilterFactory extends TokenFilterFactory implements ResourceLoaderAware
{
	public OffensiveStopWordsFilterFactory(Map<String, String> args) {
	    super(args);

	    m_removeOffensive = getBoolean(args, REMOVE_OFFENSIVE_ATTR, false);
	    m_stopWordFiles = get(args, OFFENSIVE_WORDS_ATTR);
	    m_isExact = getBoolean(args, IS_EXACT_ATTR, false);
	    
		if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

	private static final String OFFENSIVE_WORDS_ATTR = "offensive_words";
	private static final String REMOVE_OFFENSIVE_ATTR = "offensive";
	private static final String IS_EXACT_ATTR = "is_exact";

	private CharArraySet  m_offensiveWords = null;
	private boolean m_isExact = false;
	private boolean m_removeOffensive = false;
	private String m_stopWordFiles = null;

	public void inform(ResourceLoader loader)
	{
		if(!m_removeOffensive)
		{
			return;
		}

		try
		{
			m_offensiveWords = getWordSet(loader, m_stopWordFiles, true);
		}
		catch(Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	public TokenStream create(TokenStream input)
	{
		return new OffensiveStopWordsFilter(input, m_offensiveWords, m_isExact);
	}
}
