//package tracx.server.index.analysis.kstemmerAnalyzer;
//
//import java.io.IOException;
//import org.apache.lucene.analysis.TokenFilter;
//import org.apache.lucene.analysis.TokenStream;
//import org.apache.lucene.analysis.en.KstemmerWrapper;
//import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
//import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
//
//public class KStemFilter extends TokenFilter
//{
//	private final KstemmerWrapper stemmer = new KstemmerWrapper();
//	private final CharTermAttribute termAttribute = addAttribute(CharTermAttribute.class);
//	private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);
//
//	private String m_term = "";
//
//	public KStemFilter(TokenStream in)
//	{
//		super(in);
//	}
//
//	/**
//	 * Returns the next, stemmed, input Token.
//	 *
//	 * @return The stemmed form of a token.
//	 * @throws IOException
//	 */
//	@Override
//	public boolean incrementToken() throws IOException
//	{
//		int skippedPositions = 0;
//		while(input.incrementToken())
//		{
//			char[] term = termAttribute.buffer();
//			int len = termAttribute.length();
//
//			if(len == 0)
//			{
//				continue;
//			}
//
//			m_term = String.valueOf(term, 0, len);
//			String stem = stemmer.stem(m_term);
//
//			if(!m_term.equals(stem))
//			{
//				termAttribute.setEmpty().append(stem);
//				if(skippedPositions != 0)
//				{
//					posIncrAtt.setPositionIncrement(posIncrAtt.getPositionIncrement() + skippedPositions);
//				}
//
//				return true;
//			}
//
//			skippedPositions += posIncrAtt.getPositionIncrement();
//		}
//
//		return false;
//	}
//}
