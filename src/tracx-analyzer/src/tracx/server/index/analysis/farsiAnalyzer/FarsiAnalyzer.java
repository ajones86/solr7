package tracx.server.index.analysis.farsiAnalyzer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.ar.ArabicNormalizationFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import tracx.common.utils.FileUtils;

public class FarsiAnalyzer extends Analyzer
{
	private static HashSet<String> ms_nouns = new HashSet<String>();
	private static HashSet<String> ms_suffixes = new HashSet<String>();
	private static HashMap<String, String> ms_verbs = new HashMap<String, String>();

	private static final String DICTIONERIES_PATH = "./prop/dict_farsi/";
	private static final String NOUN_DICT = "nouns";
	private static final String VERB_DICT = "verbs";
	private static final String SUFFIX_DICT = "suffixes";
	private static final String SEP = "\t";

	static
	{
		try
		{
			List<String> lines = IOUtils.readLines(new FileInputStream(FileUtils.getResource(DICTIONERIES_PATH + NOUN_DICT)));
			for(String noun : lines)
			{
				noun = noun.replaceAll("ك", "ک");
				ms_nouns.add(noun);
			}
	
			lines = IOUtils.readLines(new FileInputStream(FileUtils.getResource(DICTIONERIES_PATH + SUFFIX_DICT)));
			for(String suffix : lines)
			{
				suffix = suffix.replaceAll("ك", "ک");
				ms_suffixes.add(suffix);
			}
	
			lines = IOUtils.readLines(new FileInputStream(FileUtils.getResource(DICTIONERIES_PATH + VERB_DICT)));
			for(String line : lines)
			{
				line = line.replaceAll("ك", "ک");
				String[] parts = line.split(SEP);
	
				ms_verbs.put(parts[0].trim(), parts[1].trim());
			}
		}
		catch(IOException e)
		{
			System.out.println("FarsiAnalyzer failed to load resources due to "+e.getMessage());
		}
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName)
	{
		Tokenizer tokenizer = new StandardTokenizer();
		TokenStream stream  = new ArabicNormalizationFilter(tokenizer);


		stream = FarsiFilterFactory.create(stream, ms_nouns, ms_verbs, ms_suffixes);
		
		return new TokenStreamComponents(tokenizer, stream);
		
	}

	
	public static void main(String ... args) throws Exception
	{
		Analyzer analyzer = new FarsiAnalyzer();

		String text = "سلام جهان";

		TokenStream stream =analyzer.tokenStream("", new StringReader(text));
		stream.reset();
		while(stream.incrementToken())
		{
			CharTermAttribute att = stream.getAttribute(CharTermAttribute.class);
			System.out.println(att.buffer());
		}
		stream.end();
		stream.close();
	}

}
