package tracx.server.index.analysis.farsiAnalyzer;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

public class FarsiFilter extends TokenFilter
{
	private final CharTermAttribute termAttribute = addAttribute(CharTermAttribute.class);
	private final PositionIncrementAttribute posIncrAtt = addAttribute(PositionIncrementAttribute.class);

	private static final String BAH = "ب";
	private static final String GAN = "گان";
	private static final String HAH = "ه";
	private static final String SPECIAL_YAH = "ی‌";
	private static final String YAH = "ی";
	private static final String SPECIAL_KAH = "ك";
	private static final String KAH = "ک";

	private HashMap<String, String> m_verbsDict;
	private HashSet<String> m_suffixes;
	private HashSet<String> m_nounDict;
	private boolean m_isFirst = true;

	private HashSet<String> m_termsToIndex = new HashSet<String>();

	public FarsiFilter(TokenStream input, HashSet<String> nounDict, HashMap<String, String> verbsDict, HashSet<String> suffixes)
	{
		super(input);

		m_verbsDict = verbsDict;
		m_suffixes = suffixes;
		m_nounDict = nounDict;
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		if(m_termsToIndex.isEmpty())
		{
			if(!input.incrementToken())
			{
				return false;
			}

			m_isFirst = true;
		}

		if((m_verbsDict == null) || m_verbsDict.isEmpty())
		{
			return true;
		}

		// Few replacement that are needed for better connection with the dictionary.
		String term = String.valueOf(termAttribute.subSequence(0, termAttribute.length())).replaceAll(SPECIAL_KAH, KAH).replaceAll(SPECIAL_YAH, YAH).trim();

		if(m_verbsDict.containsKey(term))
		{
			term = m_verbsDict.get(term);
			termAttribute.setEmpty().append(term);
			m_termsToIndex.clear();

			return true;
		}

		// Do suffixes stuff
		if(!m_termsToIndex.isEmpty())
		{
			posIncrAtt.setPositionIncrement(m_isFirst ? 1 : 0);
			m_isFirst = false;

			term = m_termsToIndex.iterator().next();
			m_termsToIndex.remove(term);

			termAttribute.setEmpty().append(term);
			return true;
		}

		m_termsToIndex = stripSuffixesAndPrefixes(term);

		return incrementToken();
	}

	private HashSet<String> stripSuffixesAndPrefixes(String term)
	{
		HashSet<String> stripped = new HashSet<String>();

		String subTerm = "";
		String suffix = "";
		String indexedSuffix = "";
		int length = term.length();

		for(int i = 0; (i < length) ; i++)
		{
			if((m_suffixes == null) || m_suffixes.isEmpty())
			{
				break;
			}

			if((m_nounDict == null) || m_nounDict.isEmpty())
			{
				break;
			}

			subTerm = new String(term.substring(0, length - i)).trim();
			suffix = new String(term.substring(length - i)).trim();

			if(!m_suffixes.contains(suffix) && !m_suffixes.contains(suffix.replaceAll(indexedSuffix, ""))) continue;

			if(m_nounDict.contains(subTerm))
			{
				stripped.add(subTerm);
				indexedSuffix = suffix;
			}


			// Sometimes there could be a suffix that represent plural GAN but if the term ends
			// with HAH then the GAN will replace the HAH.
			if(suffix.equals(GAN) && m_nounDict.contains(subTerm + HAH))
			{
				subTerm += HAH;
			}

			if(subTerm.startsWith(BAH))
			{
				if((subTerm.length() > 1) && m_nounDict.contains(subTerm.substring(1)))
				{
					stripped.add(subTerm.substring(1));
					indexedSuffix = suffix;
				}
			}
		}

		if(stripped.isEmpty())
		{
			// Could not found any stemming of the term so index it as is.
			stripped.add(term);
		}

		return stripped;
	}
}
