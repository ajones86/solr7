package tracx.server.index.analysis.farsiAnalyzer;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoaderAware;
import org.apache.lucene.analysis.util.TokenFilterFactory;

public class FarsiFilterFactory extends TokenFilterFactory implements ResourceLoaderAware
{
	public FarsiFilterFactory(Map<String, String> args) 
	{
	    super(args);
	    if (!args.isEmpty()) {
	      throw new IllegalArgumentException("Unknown parameters: " + args);
	    }
	}

	private static final String SEP = "\t";

	private HashMap<String, String> m_verbs = new HashMap<String, String>();
	private HashSet<String> m_nouns = new HashSet<String>();
	private HashSet<String> m_suffixes = new HashSet<String>();

	public void inform(ResourceLoader loader)
	{
		try
		{
			List<String> verbsLines = getLines(loader, "./dict_farsi/verbs");
			for(String line : verbsLines)
			{
				line = line.replaceAll("ك", "ک");
				String[] parts = line.split(SEP);
				m_verbs.put(parts[0].trim(), parts[1].trim());
			}

			List<String> nounLines = getLines(loader, "./dict_farsi/nouns");
			for(String line : nounLines)
			{
				line = line.replaceAll("ك", "ک");
				m_nouns.add(line.trim());
			}

			List<String> suffixesLines = getLines(loader, "./dict_farsi/suffixes");
			for(String line : suffixesLines)
			{
				line = line.replaceAll("ك", "ک");
				m_suffixes.add(line.trim());
			}

			m_suffixes.add("");

		}
		catch(Exception e)
		{
		}
	}

	@Override
	public TokenStream create(TokenStream input)
	{
		return new FarsiFilter(input, m_nouns, m_verbs, m_suffixes);
	}

	public static TokenStream create(TokenStream input, HashSet<String> nouns, HashMap<String, String> verbs, HashSet<String> suffixes)
	{
		return new FarsiFilter(input, nouns, verbs, suffixes);
	}
}
