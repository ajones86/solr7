package gpl.pierrick.brihaye.aramorph.lucene;

/*
Copyright (C) 2003  Pierrick Brihaye
pierrick.brihaye@wanadoo.fr

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
Free Software Foundation, Inc.
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA
or connect to:
http://www.fsf.org/copyleft/gpl.html
*/

import gpl.pierrick.brihaye.aramorph.AraMorph;
import gpl.pierrick.brihaye.aramorph.Solution;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List; 
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class ArabicStemmer extends TokenFilter {

	public final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
	public final OffsetAttribute offsetAtt = addAttribute(OffsetAttribute.class);
	public final PositionIncrementAttribute posIncAtt = addAttribute(PositionIncrementAttribute.class);

	/** Whether or not the analyzer should output debug messages */
	protected boolean debug = false;
	/** Whether or not the analyzer should output tokens in the Buckwalter transliteration system */
	protected boolean outputBuckwalter = false; //TODO : revisit default value ?
	private AraMorph araMorph = null;
	private String romanizedToken = null;
//	private Token receivedToken = null;
	private boolean processingToken = false;
	private ArrayList tokenSolutions = null;

	private String m_originalString = "";
	private boolean m_hadSolution = false;
	private List<String> m_termsCollection = new LinkedList<String>();

	public ArabicStemmer()
	{
		this(null);
	}

	/** Constructs a stemmer that will return the possible stems for arabic tokens in the Buckwalter transliteration system.
	 * @param input The token stream from a tokenizer
	 */
	public ArabicStemmer(TokenStream input) {
		this(input, false, true);
	}

	/** Constructs a stemmer that will return the possible stems for arabic tokens in the Buckwalter transliteration system.
	 * @param input The reader
	 * @param debug Whether or not the stemmer should display convenience messages on <CODE>System.out</CODE>
	 */
	public ArabicStemmer(TokenStream input, boolean debug) {
		this(input, debug, true);
	}

	/** Constructs a stemmer that will return the possible stems for arabic tokens.
	 * @param input The reader
	 * @param debug Whether or not the stemmer should display convenience messages on <CODE>System.out</CODE>
	 * @param outputBuckwalter Whether or not the analyzer should output tokens in the Buckwalter transliteration system
	 */
	public ArabicStemmer(TokenStream input, boolean debug, boolean outputBuckwalter) {
		super(input);
		this.debug = debug;
		this.outputBuckwalter = outputBuckwalter;
		araMorph = new AraMorph();
	}

	/** Returns the arabic stemmer in use.
	 * @return The arabic stemmer
	 * @see gpl.pierrick.brihaye.aramorph.AraMorph
	 */
	public AraMorph getAramorph() { return araMorph; }

	/** Returns the next stem for the given token.
	 * @param firstOne Whether or not this stem is the first one
	 * @return The token. Its <CODE>termText</CODE> is arabic <STRONG>stem</STRONG>. Its <CODE>type</CODE> is the grammatical category of the <STRONG>stem</STRONG>.
	 * When several stems are available, every emitted token's
	 * <CODE>PositionIncrement</CODE> but the first one is set to <CODE>0</CODE>
	 * @see org.apache.lucene.analysis.Token#setPositionIncrement(int)
	 */
	private String nextSolution() {
		String tokenText = null;
		String tokenType = null;
		try {
			//Get the first solution
			Solution currentSolution = (Solution)tokenSolutions.get(0);
			//This is the trick ! Only the canonical form of the stem is to be considered
			if (outputBuckwalter)
//				tokenText = currentSolution.getStemVocalization(); //TODO : should we use the "entry" ?
				tokenText = AraMorph.arabizeWord(currentSolution.getLemma());			   // TODO: Index both?

			else
//				tokenText = currentSolution.getStemArabicVocalization(); //TODO : should we use the "entry" ?
				tokenText = AraMorph.arabizeWord(currentSolution.getLemma());			   // TODO: Index both?
			if (tokenText == null) tokenText = "";
			//Token is typed in order to filter it later
			tokenType = currentSolution.getStemPOS();
			if (tokenType == null) tokenType = "NO_STEM";
			//OK : we're done with this solution
			tokenSolutions.remove(0);
			//Will there be further treatment ?
			processingToken = !tokenSolutions.isEmpty();
		}
		//It should not be normally possible !
		catch (IndexOutOfBoundsException e) {
			System.err.println("Something went wrong in nextSolution");
			processingToken = false;
			//Re-emit the same token text (romanized) : not the best solution :-(
			tokenText = romanizedToken;
			tokenType = "PLACE_HOLDER";
		}
//		emittedToken = new Token(tokenText,receivedToken.startOffset(),receivedToken.endOffset(),tokenType);
//		setAttribute(tokenText, tokenType, firstOne);
		//		if (!firstOne) emittedToken.setPositionIncrement(0);
//		if (debug) System.out.println(emittedToken.termText() + "\t" + emittedToken.type() + "\t" + "[" + emittedToken.startOffset() + "-" + emittedToken.endOffset() + "]" + "\t" + emittedToken.getPositionIncrement());
		return tokenText;
	}

	/** Returns the next token in the stream, or <CODE>null</CODE> at EOS.
	 * @throws IOException If a problem occurs
	 * @return The token with its <CODE>type</CODE> set to the morphological identification of the
	 * <STRONG>stem</STRONG>. Tokens with no grammatical identification have their <CODE>type</CODE> set to
	 * <CODE>NO_RESULT</CODE>. Token's termText is the romanized form of the
	 * <STRONG>stem</STRONG>
	 * @see org.apache.lucene.analysis.Token#type()
	 */
	public final boolean next() throws IOException {
		//If no token is currently processed, fetch another one
		String token = "";
		if (!processingToken) {
//			receivedToken = input.next();
			if ((m_originalString == null) || m_originalString.equals("")) return false;
			romanizedToken = araMorph.romanizeWord(m_originalString);
			//Analyse it (in arabic)
			if (araMorph.analyzeToken(m_originalString, romanizedToken, outputBuckwalter)) {
				tokenSolutions = new ArrayList(araMorph.getWordSolutions(romanizedToken));

				//DEBUG : this does actually nothing, good place for a breakpoint
				if (tokenSolutions.isEmpty()) { //oh, no !
					tokenSolutions.clear();
					processingToken = false;
					return false;
				}

				token = getNonEmptyNextSolution();
				if(isEmptyOrNull(token)) return false;

				setAttribute(token, true);
			}
			else {
				processingToken = false;
				if (debug) System.out.println(romanizedToken + "\t" + "NO_RESULT");
				return false;//new Token(romanizedToken,receivedToken.startOffset(),receivedToken.endOffset(),"NO_RESULT");
			}
		}
		else 		//Continue to process the current token
		{
			token = getNonEmptyNextSolution();
			if(isEmptyOrNull(token)) return false;

			setAttribute(token, false);
		}

		return true;
	}

	private String getNonEmptyNextSolution()
	{
		String token = nextSolution();
		while(isEmptyOrNull(token) && !tokenSolutions.isEmpty())
		{
			token = nextSolution();
		}

		return token;
	}

	@Override
	public boolean incrementToken() throws IOException
	{
		// Normalize so that no duplicated (Regardless to NIKOD) will be stored.
		if(!processingToken)
		{
			m_hadSolution = false;
			if(!input.incrementToken())
			{
				return false;
			}

			m_originalString = new String(termAtt.buffer()).substring(0, termAtt.length());
			if(!isArabicTerm(m_originalString)) return true;
		}

		if((m_termsCollection == null) || m_termsCollection.isEmpty())
		{
			if(next())
			{
				m_hadSolution = true;	// means that AraMorph found a solution for it
				return true;
			}
			else if(m_hadSolution)	// We're just out of solutions so no need to lightstem it.
			{
				return incrementToken();
			}

			m_termsCollection = getTermsCollection(m_originalString);
		}

		String term = m_termsCollection.iterator().next();

		setAttribute(term, !processingToken);
		processingToken = true;
		m_termsCollection.remove(term);

		if((m_termsCollection == null) || m_termsCollection.isEmpty())
		{
			processingToken = false;
		}

		return true;
	}

	private List<String> getTermsCollection(String rawTerm)
	{
		LinkedList<String> res = new LinkedList<String>();
		res.add(rawTerm);

		// TODO MOSHE - CHANGE THE HELL OUT OF IT!!!
		ArabicLightStemmer stemmer = new ArabicLightStemmer();
		char charArr[] = rawTerm.toCharArray();
		int lemmasLength = stemmer.stem(charArr, m_originalString.length());

		if(lemmasLength != rawTerm.length())
		{
			res.add(String.valueOf(charArr, 0, lemmasLength));
		}

		return res;
	}

	public void setAttribute(String term, boolean allowPositionIncrement)
	{
		// Keeping the current start offset before clearing the attribute.
		int startOffset = offsetAtt.startOffset();

		clearAttributes();
		termAtt.copyBuffer(term.toCharArray(), 0, term.length());

		termAtt.setLength(term.length());
		offsetAtt.setOffset(startOffset, startOffset + term.length());
		posIncAtt.setPositionIncrement(allowPositionIncrement?1:0);
	}

	public static boolean isArabicTerm(String term)
	{
		int maxCharsToCheck = 100;
		Character.UnicodeBlock block = null;
		for(int i = 0 ; ((i < term.length()) && (i < maxCharsToCheck)) ; i++)
		{
			char letter = term.charAt(i);
			block = Character.UnicodeBlock.of(letter);
			if((block != null) && block.toString().contains("ARABIC"))
			{
				return true;
			}
		}

		return false;
	}

	private boolean isEmptyOrNull(String text)
	{
		return ((text == null) || text.isEmpty() || text.equals(""));
	}
}


